#include "Child.h"

Child::Child(std::function<void(const std::string&)> callback, const std::string& name) :
	m_callbackWithParams(callback),
	m_name(name)
{
}

Child::Child(std::function<void()> callback, const std::string& name) :
	m_callback(callback),
	m_name(name)
{
}

void Child::doSomenthingFromChild()
{
	m_callback();
}

void Child::doSomethingFromChildWithParams()
{
	m_callbackWithParams(m_name);
}
