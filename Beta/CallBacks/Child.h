#pragma once
#include <functional>
#include <string>

class Child {
public:
	Child(std::function<void(const std::string&)> callback, const std::string& name);
	Child(std::function<void()> callback, const std::string& name);

	void doSomethingFromChildWithParams();
	void doSomenthingFromChild();
private:
	std::function<void(const std::string&)> m_callbackWithParams;
	std::function<void()> m_callback;
	std::string m_name;
};
