#pragma once

#include "Child.h"

#include <vector>
#include <memory>
#include <iostream>

class Parent {
public:
	Parent();
	std::vector<Child> components;
private:
	void doSomethingWithParams(const std::string& name);
	void doSomething();
};

