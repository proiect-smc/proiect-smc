#include "Parent.h"

Parent::Parent() {
	std::function<void(const std::string&)> callbackWithParams = std::bind(&Parent::doSomethingWithParams, this, std::placeholders::_1);
	for (auto i = 0; i < 3; ++i) {
		components.push_back(Child(callbackWithParams, "copil " + std::to_string(i)));
	}

	std::function<void()> callback = std::bind(&Parent::doSomething, this);
	for (auto i = 0; i < 3; ++i) {
		components.push_back(Child(callback, "copil " + std::to_string(i)));
	}

}

void Parent::doSomethingWithParams(const std::string& name)
{
	std::cout << "Done "<<name<<"!\n";
}

void Parent::doSomething()
{
	std::cout << "Done!\n";
}
