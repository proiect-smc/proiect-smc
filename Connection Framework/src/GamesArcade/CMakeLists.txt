message("GamesArcade cmake file")

set(INCROOT ${PROJECT_SOURCE_DIR}/include/GamesArcade)
set(SRCROOT ${PROJECT_SOURCE_DIR}/src/GamesArcade)

set(INCROOTHEX ${PROJECT_SOURCE_DIR}/include/GamesArcade/Hex)
set(SRCROOTHEX ${PROJECT_SOURCE_DIR}/src/GamesArcade/Hex)

set(INCROOTY ${PROJECT_SOURCE_DIR}/include/GamesArcade/Y)
set(SRCROOTY ${PROJECT_SOURCE_DIR}/src/GamesArcade/Y)

set(INCROOTHAVANNAH ${PROJECT_SOURCE_DIR}/include/GamesArcade/Havannah)
set(SRCROOTHAVANNAH ${PROJECT_SOURCE_DIR}/src/GamesArcade/Havannah)

file(GLOB INCS "${INCROOT}/*.h")
file(GLOB SRCS "${SRCROOT}/*.cpp")

file(GLOB INCSHEX "${INCROOTHEX}/*.h")
file(GLOB SRCSHEX "${SRCROOTHEX}/*.cpp")

file(GLOB INCSY "${INCROOTY}/*.h")
file(GLOB SRCSY "${SRCROOTY}/*.cpp")

file(GLOB INCSHAVANNAH "${INCROOTHAVANNAH}/*.h")
file(GLOB SRCSHAVANNAH "${SRCROOTHAVANNAH}/*.cpp")

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${ConnectionGamesFramework_SOURCE_DIR}/bin")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${ConnectionGamesFramework_SOURCE_DIR}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${ConnectionGamesFramework_SOURCE_DIR}/bin")

include_directories("${ConnectionGamesFramework_SOURCE_DIR}/bin")

add_library(GamesArcade SHARED ${SRCS} ${INCS} ${SRCSHEX} ${INCSHEX} ${SRCSY} ${INCSY} ${SRCSHAVANNAH} ${INCSHAVANNAH})

include(GenerateExportHeader)

generate_export_header(GamesArcade EXPORT_FILE_NAME ${CMAKE_BINARY_DIR}/exports/GamesArcade_export.h)

include_directories(${CMAKE_BINARY_DIR}/exports)

target_link_libraries(GamesArcade Framework)