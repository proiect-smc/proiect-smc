#include <GamesArcade/Y/Triangle.h>

Triangle::Triangle(const Callback& placePieceCallback, const Callback& selectPieceCallback, const size_t& edgeSize)
    : HexGrid(placePieceCallback, selectPieceCallback, edgeSize)
{
    int int_edgeSize = static_cast<int>(edgeSize);
    for (int q = 0; q <= int_edgeSize; ++q) {
        for (int r = int_edgeSize - q; r <= int_edgeSize; ++r) {
            AddPiece(q, r);
        }
    }
}

std::vector<GridNode> Triangle::GetCorners() const
{
    int gridSize = static_cast<int>(size);
    return {
            {gridSize, 0},
            {0,        gridSize},
            {gridSize, gridSize}
    };
}

bool Triangle::IsInBoard(const int& line, const int& column) const
{
    int boardSize = static_cast<int>(size);

    if (line < 0 || line > boardSize
        || column < boardSize - line || column > boardSize)
        return false;
    return true;
}

bool Triangle::CheckYConnection(const GridNode& lastPiecePlaced, ConsiderCorners considerCorners) const
{
    return BoardGrid::CheckYConnection(lastPiecePlaced, ConsiderCorners::TRUE);
}