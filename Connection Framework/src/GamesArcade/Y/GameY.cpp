#include <GamesArcade/Y/GameY.h>

GameY::GameY(const std::string& gameName) :
        Game(gameName)
{
    SetBoardSize(12);

    AddPlayer("Player1", Player::Color::BLACK);
    AddPlayer("Player2", Player::Color::WHITE);
}

bool GameY::WinCondition() const
{
    GridNode lastPiecePlaced = GetLastPiece(*m_currentPlayer);

    return m_board->CheckYConnection(lastPiecePlaced, BoardGrid::ConsiderCorners::TRUE);
}

void GameY::ConfigureBoard() 
{
    Game::ConfigureBoard();
    m_board = std::make_unique<Triangle>(m_placePieceCallback, m_selectPieceCallback, m_boardSize);
}
