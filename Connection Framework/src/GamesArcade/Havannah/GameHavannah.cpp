#include <GamesArcade/Havannah/GameHavannah.h>

GameHavannah::GameHavannah(const std::string& gameName) :
        Game(gameName)
{
    SetBoardSize(9);

	AddPlayer("Player1", Player::Color::BLACK);
	AddPlayer("Player2", Player::Color::WHITE);
}

bool GameHavannah::WinCondition() const
{
	GridNode piece = GetLastPiece(*m_currentPlayer);
	HexPoint lastPiecePlaced = static_cast<HexPoint>(piece);

	bool yCheck = m_board->CheckYConnection(lastPiecePlaced, BoardGrid::ConsiderCorners::FALSE);
	bool ringcheck = m_board->CheckRingConnection(lastPiecePlaced);
	bool bridgeCheck = m_board->CheckBridgeConnection(*m_currentPlayer);

	return yCheck || ringcheck || bridgeCheck;
}

void GameHavannah::ConfigureBoard() 
{
	Game::ConfigureBoard();
	m_board = std::make_unique<Hexagon>(m_placePieceCallback, m_selectPieceCallback, m_boardSize);
}
