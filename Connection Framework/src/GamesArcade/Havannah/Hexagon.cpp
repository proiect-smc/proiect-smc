#include <GamesArcade/Havannah/Hexagon.h>

Hexagon::Hexagon(const Callback& placePieceCallback, const Callback& selectPieceCallback, const size_t& edgeSize)
    : HexGrid(placePieceCallback, selectPieceCallback, edgeSize)
{
    int hexGridSize = static_cast<int>(edgeSize);
    for (int q = -hexGridSize; q <= hexGridSize; ++q)
    {
        int r1 = std::max(-hexGridSize, -q - hexGridSize);
        int r2 = std::min(hexGridSize, -q + hexGridSize);

        for (int r = r1; r <= r2; ++r)
        {
            AddPiece(q, r);
        }
    }
}

std::vector<GridNode> Hexagon::GetCorners() const
{
    int gridSize = static_cast<int>(size);
    return {
            {0,         -gridSize},
            {gridSize,  -gridSize},
            {gridSize,  0},
            {0,         gridSize},
            {-gridSize, gridSize},
            {-gridSize, 0}
    };
}

bool Hexagon::IsInBoard(const int& line, const int& column) const
{
    int boardSize = static_cast<int>(size);

    int r1 = std::max(-boardSize, -line - boardSize);
    int r2 = std::min(boardSize, -line + boardSize);

    if (line < -boardSize || line > boardSize
        || column < r1 || column > r2)
        return false;
    return true;
}

bool Hexagon::CheckBridgeConnection(const std::shared_ptr<Player> &player) const
{
    auto corners = GetCorners();
    for (int indexCorner1 = 0; indexCorner1 < corners.size() - 1; ++indexCorner1)
        for (int indexCorner2 = indexCorner1 + 1; indexCorner2 < corners.size(); ++indexCorner2)
            if (BFS(corners[indexCorner1], corners[indexCorner2], player)) {
                std::cout << "Bridge connection\n";
                return true;
            }
    return false;
}

bool Hexagon::CheckRingConnection(const GridNode& lastPiecePlaced) const
{
    std::vector<GridNode> pointsVector;
    std::shared_ptr<Player> player = at(lastPiecePlaced).GetOwner().value();

    pointsVector = BFS(lastPiecePlaced, player);

    const int minimumNumberOfNeighbors = 4;
    bool modified = true;
    while (modified)
    {
        modified = false;

        for (int index = 0; index < pointsVector.size(); ++index)
        {
            int numberOfDiferentNeighbors = FindNumberOfDifferentNeighbors(pointsVector, pointsVector[index]);

            if (numberOfDiferentNeighbors == minimumNumberOfNeighbors)
            {
                pointsVector.erase(pointsVector.begin() + index);
                modified = true;
                break;
            }
        }
    }

    if (CheckIfHexPointHasSixSameNeighbors(pointsVector))
    {
        std::cout << "Ring connection!\n";
        return true;
    }

    if (pointsVector.size() != 0)
    {
        if (CheckMiddleCellInRing(pointsVector, player))
        {
            std::cout << "Ring connection!\n";
            return true;
        }
        pointsVector.clear();
    }

    return false;
}

bool Hexagon::CheckYConnection(const GridNode& lastPiecePlaced, ConsiderCorners considerCorners) const
{
    return BoardGrid::CheckYConnection(lastPiecePlaced, ConsiderCorners::FALSE);
}

bool Hexagon::CheckHexPointIsInConnexComponent(const std::vector<GridNode>& pointsVector, const GridNode& pointToCheck) const
{
    return std::any_of(pointsVector.cbegin(), pointsVector.cend(), [&pointToCheck](const auto& point) { return point == pointToCheck; });
}

int Hexagon::FindNumberOfDifferentNeighbors(const std::vector<GridNode>& pointsVector, const GridNode& point) const
{
    int numberOfDiferentNeighbors = 0;
    const int minimumNumberOfNeighbors = 4;
    const int numberOfIterationNeeded = 2; // HEX_NEIGHBORS vector will be iterated twice for 
                                           // findind how many consecutive neighbors are not owned by the same player

    for (int number = 0; number < numberOfIterationNeeded; ++number)
    {
        for (const auto& neighborAdder : NeighborModifiers())
        {
            GridNode neighbor = point + neighborAdder;

            if (!CheckHexPointIsInConnexComponent(pointsVector, neighbor))
            {
                ++numberOfDiferentNeighbors;
                if (numberOfDiferentNeighbors == minimumNumberOfNeighbors)
                    return numberOfDiferentNeighbors;
            }
            else
                numberOfDiferentNeighbors = 0;
        }
    }
    return numberOfDiferentNeighbors;
}

bool Hexagon::CheckIfHexPointHasSixSameNeighbors(const std::vector<GridNode>& pointsVector) const
{
    for (const auto& point : pointsVector)
    {
        int numberOfNeighbors = 0;
        for (const auto& neighborAdder : NeighborModifiers())
        {
            GridNode neighbor = point + neighborAdder;
            if (CheckHexPointIsInConnexComponent(pointsVector, neighbor))
                ++numberOfNeighbors;
        }

        if (numberOfNeighbors == NeighborModifiers().size())
            return true;
    }
    return false;
}

bool Hexagon::CheckMiddleCellInRing(const std::vector<GridNode>& pointsVector, const std::shared_ptr<Player> player) const
{
    for (const auto& neighborAdder : NeighborModifiers())
    {
        GridNode neighbor = pointsVector[0] + neighborAdder;
        try
        {
            if (!at(neighbor).GetOwner().has_value() ||
                at(neighbor).GetOwner().has_value() && at(neighbor).GetOwner().value() != player)
            {
                GridNode nextNeighbor = neighbor + neighborAdder;
                while (true)
                {
                    if (!IsInBoard(nextNeighbor.x, nextNeighbor.y))
                        break;

                    if (CheckHexPointIsInConnexComponent(pointsVector, nextNeighbor))
                        return true;

                    nextNeighbor = nextNeighbor + neighborAdder;
                }
            }
        }
        catch (const std::exception& exception)
        {
            continue;
        }
    }
    return false;
}