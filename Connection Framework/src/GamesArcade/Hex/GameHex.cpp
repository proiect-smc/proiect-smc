#include <GamesArcade/Hex/GameHex.h>

GameHex::GameHex(const std::string& gameName) :
        Game(gameName)
{
	SetBoardSize(9);

	AddPlayer("Player1", Player::Color::BLUE);
	AddPlayer("Player2", Player::Color::RED);
}

bool GameHex::WinCondition() const
{
	return m_board->CheckBridgeConnection(*m_currentPlayer);
}

void GameHex::ConfigureBoard()
{
	SetBoardSize(GetBoardSize() + 2);

	Game::ConfigureBoard();
	m_board = std::make_unique<Parallelogram>(m_placePieceCallback, m_selectPieceCallback, m_boardSize);

	auto edges = m_board->GetEdges(BoardGrid::ConsiderCorners::FALSE);
	for (int edgeIndex = 0; edgeIndex < edges.size(); ++edgeIndex)
		for (int pointIndex = 0; pointIndex < edges[edgeIndex].size(); ++pointIndex)
		{
			m_board->at(edges[edgeIndex][pointIndex]).SetOwner(GetPlayerAt(edgeIndex % 2));
			m_board->at(edges[edgeIndex][pointIndex]).SetAsMarker();
		}

	auto corners = m_board->GetCorners();
	for (const auto& corner : corners)
		m_board->erase(corner);
}