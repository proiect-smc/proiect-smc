#include <GamesArcade/Hex/Parallelogram.h>

Parallelogram::Parallelogram(const Callback& placePieceCallback, const Callback& selectPieceCallback, const size_t& edgeSize)
    : HexGrid(placePieceCallback, selectPieceCallback, edgeSize)
{
    for (int q = 0; q <= edgeSize; ++q) {
        for (int r = 0; r <= edgeSize; ++r) {
            AddPiece(q, r);
        }
    }
}

std::vector<GridNode> Parallelogram::GetCorners() const
{
    int gridSize = static_cast<int>(size);
    return {
            {0,        0},
            {gridSize, 0},
            {gridSize, gridSize},
            {0,        gridSize}
    };
}

bool Parallelogram::IsInBoard(const int& line, const int& column) const
{
    int boardSize = static_cast<int>(size);

    if (line < 0 || line > boardSize
        || column < 0 || column > boardSize)
        return false;
    return true;
}

bool Parallelogram::CheckBridgeConnection(const std::shared_ptr<Player> &player) const
{
    std::vector<GridNode> startPoints;
    std::vector<GridNode> endPoints;

    auto edges = GetEdges(ConsiderCorners::FALSE);
    bool foundFirstEdge = false;
    for (const auto& edge : edges)
        for (const auto& pointEdge : edge)
            if (at(pointEdge).GetOwner().has_value())
                if (at(pointEdge).GetOwner().value() == player) {
                    if (!foundFirstEdge) {
                        foundFirstEdge = true;
                        startPoints = edge;
                        break;
                    } else
                        endPoints = edge;
                }

    for (const auto& startPoint : startPoints)
        for (const auto& endPoint : endPoints)
        {
            if (BFS(startPoint, endPoint, player)) {
                std::cout << "Bridge connection\n";
                return true;
            }
        }
    return false;
}