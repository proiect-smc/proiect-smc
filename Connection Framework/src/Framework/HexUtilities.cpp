#include <Framework/HexUtilities.h>

GridNode::GridNode(int x_, int y_) noexcept :
    x(x_),
    y(y_)
{
    //empty
}

GridNode::GridNode(const GridNode& another)
{
    *this = another;
}

GridNode::GridNode(GridNode&& another) noexcept
{
    *this = std::move(another);
}

GridNode& GridNode::operator=(const GridNode& another)
{
    if (this != &another)
    {
        x = another.x;
        y = another.y;
    }
    return *this;
}

GridNode& GridNode::operator=(GridNode&& another) noexcept
{
    if (this != &another)
    {
        int resetValue = 0;
        x = std::exchange(another.x, resetValue);
        y = std::exchange(another.y, resetValue);
    }

    return *this;
}

GridNode::~GridNode()
{
    //empty body
}

bool GridNode::operator==(const GridNode& other) const
{
    return x == other.x && y == other.y;
}

bool GridNode::operator!=(const GridNode& other) const
{
    return x != other.x || y != other.y;
}

GridNode GridNode::operator+(const GridNode& other) const
{
    return GridNode(x + other.x, y + other.y);
}

GridNode GridNode::operator-(const GridNode& other) const
{
    return GridNode(x - other.x, y - other.y);
}

GridNode GridNode::operator*(const int k) const
{
    return GridNode(x * k, y * k);
}

GridNode GridNode::operator/(const int k) const {
    if (k == 0) throw "Divide by Zero exception";
    return GridNode(x / k, y / k);
}

GridNode GridNode::operator+=(const GridNode& other) {
    *this = *this + other;
    return *this;
}

bool GridNode::operator<(const GridNode& other) const
{
    if (x < other.x) return true;
    if (x == other.x && y < other.y) return true;
    return false;
}

const std::vector<GridNode>  GridNode::NeighborModifiers() {
    throw "Not implemented!";
}

const std::vector<GridNode> HexPoint::NeighborModifiers() {
    return {
            HexPoint(1, 0), HexPoint(1, -1), HexPoint(0, -1),
            HexPoint(-1, 0), HexPoint(-1, 1), HexPoint(0, 1)
    };
}

HexPoint::HexPoint(int x_, int y_) :
        GridNode(x_, y_) {
}

HexPoint::HexPoint() : GridNode() {}

HexPoint::HexPoint(const GridNode& node) {
    x = node.x;
    y = node.y;
}

HexPoint::HexPoint(const HexPoint& another)
{
    *this = another;
}

HexPoint::HexPoint(HexPoint&& another) noexcept
{
    *this = std::move(another);
}

HexPoint& HexPoint::operator=(const HexPoint& another)
{
    if (this != &another)
    {
        x = another.x;
        y = another.y;
    }
    return *this;
}

HexPoint& HexPoint::operator=(HexPoint&& another) noexcept
{
    if (this != &another)
    {
        int resetValue = 0;
        x = std::exchange(another.x, resetValue);
        y = std::exchange(another.y, resetValue);
    }

    return *this;
}

HexPoint::~HexPoint()
{
    //empty body
}

int HexPoint::z() const noexcept {
    return -x - y;
}
