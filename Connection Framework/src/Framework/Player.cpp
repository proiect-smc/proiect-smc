#include <Framework/Player.h>

#include <utility>

Player::Player(std::string  name, const Color& color)
    : m_name{std::move( name )}, m_color{ color }
{
}

Player::Color Player::GetColor() const noexcept
{
    return m_color;
}

std::ostream& operator<<(std::ostream& out, const Player& player)
{
    out << player.m_name;
    return out;
}

bool Player::operator==(const Player &other) const
{
    return m_color == other.m_color && m_name == other.m_name;
}

const std::string& Player::GetPlayerName() const noexcept
{
    return m_name;
}

std::unordered_map<std::string, Player::Color> Player::GetPlayerColors()
{
    std::unordered_map<std::string, Player::Color> possibleColors;

    uint8_t currentColorIndex = 0;
    auto currentColor = static_cast<Player::Color>(currentColorIndex);

    while (currentColor < Player::Color::COUNT)
    {
        possibleColors[Player::ColorToString(currentColor)] = currentColor;
        ++currentColorIndex;
        currentColor = static_cast<Player::Color>(currentColorIndex);
    }

    return possibleColors;
}

void Player::SetColor(const Color& color)
{
    m_color = color;
}

void Player::SetName(const std::string& name)
{
    m_name = name;
}

std::string Player::ColorToString(const Player::Color& color)
{
    switch (color) {
    case Player::Color::BLACK:
        return "black";
    case Player::Color::WHITE:
        return "white";
    case Player::Color::BLUE:
        return "blue";
    case Player::Color::RED:
        return "red";
    case Player::Color::GREEN:
        return "green";
    case Player::Color::MAGENTA:
        return "magenta";
    case Player::Color::CYAN:
        return "cyan";
    case Player::Color::YELLOW:
        return "yellow";
    default:
        throw "Color not defined.\n";
    }
}

Player::Player(const Player& another) {
    *this = another;
}
Player::Player(Player&& another) noexcept {
    *this = std::move(another);
}


Player& Player::operator=(const Player& another) {
    if (this != &another)
    {
        m_name = another.m_name;
        m_color = another.m_color;
    }
    return *this;
}

Player& Player::operator=(Player&& another) noexcept {

    if (this != &another)
    {
        m_name = std::exchange(another.m_name, "");
        m_color = std::exchange(another.m_color, Color::NONE);
    }
    return *this;

}

Player::~Player() {

}