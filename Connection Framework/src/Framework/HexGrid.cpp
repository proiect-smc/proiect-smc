#include <Framework/HexGrid.h>

HexGrid::HexGrid(const Callback& placePieceCallback, const Callback& selectPieceCallback, const size_t& edgeSize)
    : BoardGrid(placePieceCallback, selectPieceCallback, edgeSize)
{

}

std::vector<GridNode> HexGrid::NeighborModifiers() const
{
	return HexPoint::NeighborModifiers();
}

bool HexGrid::IsOnEdge(const GridNode& point) const
{
    int boardSize = static_cast<int>(size);

    HexPoint converted(point);

    return (converted.x == boardSize || converted.y == boardSize || converted.z() == boardSize ||
        converted.x == -boardSize || converted.y == -boardSize || converted.z() == -boardSize);
}

void HexGrid::AddPiece(const int q, const int r)
{
    HexPoint newHexPoint(q, r);
    (*this)[newHexPoint] = Piece();
    (*this)[newHexPoint].SetPoint(HexPoint(q, r));  //Hexes are assigned their respective HexPoints
    (*this)[newHexPoint].SetPlacePieceCallback(m_placePieceCallback);
    (*this)[newHexPoint].SetSelectPieceCallback(m_selectPieceCallback);
}