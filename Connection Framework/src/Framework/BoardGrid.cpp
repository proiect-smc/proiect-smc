#include <Framework/BoardGrid.h>

BoardGrid::BoardGrid(Callback placePieceCallback, Callback selectPieceCallback, const size_t& size_) :
    m_placePieceCallback(std::move(placePieceCallback)),
    m_selectPieceCallback(std::move(selectPieceCallback)),
    size(size_)
{

}

bool BoardGrid::IsBoardFull() const {
    return std::none_of(cbegin(), cend(), 
        [](const auto& pair)
        {
            const auto& [gridPoint, piece] = pair;
            return !piece.GetOwner().has_value();
        }
    );
}

void BoardGrid::Restart() {
    std::for_each(begin(), end(), 
        [](auto& pair)
        {
            auto& [gridPoint, piece] = pair;
            if (piece.GetOwner().has_value())
                piece.RemoveOwner();
        }
    );
}

std::vector<std::vector<GridNode>> BoardGrid::GetEdges(ConsiderCorners considerCorners) const {
    std::vector<std::vector<GridNode>> returnVector;
    const auto cornersVector = GetCorners();

    for (auto cornerIndex = 0; cornerIndex < cornersVector.size(); ++cornerIndex) {
        const auto& thisCorner = cornersVector[cornerIndex];
        const auto& nextCorner = cornersVector[(1 + cornerIndex) % cornersVector.size()];
        const auto& adder = (nextCorner - thisCorner) / static_cast<int>(size);

        std::vector<GridNode> edgeVector;

        if(considerCorners == ConsiderCorners::TRUE)
            edgeVector.push_back(thisCorner);

        for (GridNode pivot = GridNode(thisCorner + adder); pivot != nextCorner; pivot += adder) {
            edgeVector.push_back(pivot);
        }

        if (considerCorners == ConsiderCorners::TRUE)
            edgeVector.push_back(nextCorner);

        returnVector.push_back(edgeVector);
    }

    return returnVector;
}

bool BoardGrid::BFS(const GridNode& start, const GridNode& end, const std::shared_ptr<Player>& player) const
{

    if (!at(start).GetOwner().has_value() ||
        at(start).GetOwner().has_value() &&
        at(start).GetOwner().value() != player)
        return false;

    std::vector<GridNode> visited;
    std::queue<GridNode> pointsQueue;

    pointsQueue.push(start);
    visited.push_back(start);

    while (!pointsQueue.empty())
    {
        auto currentPoint = pointsQueue.front();
        if (currentPoint == end)
            return true;

        pointsQueue.pop();

        for (const auto& neighborAdder : NeighborModifiers())
        {
            auto neighbor = currentPoint + neighborAdder;

            try
            {
                auto IsVisited = [&visited](const GridNode& point)
                {
                    return std::any_of(visited.cbegin(), visited.cend(),
                        [&point](const auto& gridPoint)
                        {
                            return point == gridPoint;
                        }
                    );
                };

                if (IsInBoard(neighbor.x, neighbor.y) && !IsVisited({ neighbor.x, neighbor.y }) &&
                    at(neighbor).GetOwner().has_value())
                    if (at(neighbor).GetOwner().value() == player)
                    {
                        visited.push_back(neighbor);
                        pointsQueue.push(neighbor);
                    }
            }
            catch (const std::exception& exception)
            {
                continue;
            }
        }
    }
    return false;
}

std::vector<GridNode> BoardGrid::BFS(const GridNode& start, const std::shared_ptr<Player>& player) const
{
    std::map<GridNode, bool> visitedPoints;
    std::vector<GridNode> pointsVector;

    Piece lookupValue;
    try {
        lookupValue = at(start);
    }
    catch (const std::exception& exception) {
        throw exception;
    }

    std::queue<GridNode> visitingQueue;

    visitingQueue.push(start);
    visitedPoints[start] = true;

    while (!visitingQueue.empty()) {
        auto currentPoint = visitingQueue.front();
        pointsVector.push_back(currentPoint);
        visitingQueue.pop();

        for (const auto& neighborAdder : NeighborModifiers()) {
            auto neighbor = currentPoint + neighborAdder;
            try {
                if (at(neighbor) == lookupValue && !visitedPoints[neighbor] && at(neighbor).GetOwner().has_value())
                    if (at(neighbor).GetOwner().value() == player) {
                        visitedPoints[neighbor] = true;
                        visitingQueue.push(neighbor);
                    }
            }
            catch (const std::exception& exception) {
                continue;   // .at() throws exception if map does not include value
            }
        }
    }
    return pointsVector;
}

bool BoardGrid::IsCorner(const GridNode& point) const
{
    auto corners = GetCorners();
    return std::any_of(corners.cbegin(), corners.cend(), [&point](const auto& corner) { return point == corner; });
}

bool BoardGrid::IsOnSameEdge(const GridNode& pointToCheck, const GridNode& pointOnEdge) const
{
    auto edgeForPoint = FindEdgeForPoint(pointToCheck);
    return std::any_of(edgeForPoint.cbegin(), edgeForPoint.cend(), [&pointOnEdge](const auto& point) { return pointOnEdge == point; });
}

std::vector<GridNode> BoardGrid::FindEdgeForPoint(const GridNode& point) const
{
    auto edges = GetEdges(ConsiderCorners::TRUE);
    auto positon = std::find_if(edges.cbegin(), edges.cend(), [&point](const auto& edge) 
        {
            return std::any_of(edge.cbegin(), edge.cend(), [&point](const auto& pointEdge) { return point == pointEdge; });
        }
    );

    return *positon;
}

std::vector<std::vector<GridNode>> BoardGrid::FindEdgesForCorner(const GridNode& point) const
{
    std::vector<std::vector<GridNode>> edgesForCorner;
    auto edges = GetEdges(ConsiderCorners::TRUE);

    std::for_each(edges.cbegin(), edges.cend(), [&point, &edgesForCorner](const auto& edge)
        {
            if(std::any_of(edge.cbegin(), edge.cend(), [&point](const auto& pointEdge) { return point == pointEdge; }))
                edgesForCorner.push_back(edge);
        }
    );

    return edgesForCorner;
}

bool BoardGrid::CornerIsOnSameEdges(const GridNode& corner, const GridNode& point1, const GridNode& point2) const
{
    auto edgesForCorner = FindEdgesForCorner(corner);

    int numberOfPointsFounded = 0;

    for (const auto& edgeCorner : edgesForCorner)
        for (const auto& edgePoint : edgeCorner) 
            if (point1 == edgePoint || point2 == edgePoint)
            {
                ++numberOfPointsFounded;
                if (numberOfPointsFounded == edgesForCorner.size())
                    return true;
                break;
            }
    return false;
}

bool BoardGrid::CheckYConnection(const GridNode& lastPiecePlaced, ConsiderCorners considerCorners) const
{
    int intSize = static_cast<int>(size);
    const GridNode PointNotInBoard(-intSize - 1, intSize + 1);
    GridNode a = PointNotInBoard;
    GridNode b = PointNotInBoard;

    std::shared_ptr<Player> player = at(lastPiecePlaced).GetOwner().value();

    auto pointsVector = BFS(lastPiecePlaced, player);

    for (const auto& point : pointsVector)
    {
        if (IsCorner(point) && considerCorners==ConsiderCorners::FALSE)
            continue;
        if (IsOnEdge(point))
        {
            if (a == PointNotInBoard)
            {
                a = point;
                continue;
            }
            if (b == PointNotInBoard)
                if (IsCorner(point) || IsCorner(a) || !IsOnSameEdge(point, a))
                {
                    b = point;
                    continue;
                }
            if (b != PointNotInBoard)
                if (!(IsCorner(point) && CornerIsOnSameEdges(point, a, b) ||
                    (!IsCorner(point) && (IsOnSameEdge(point, a) || IsOnSameEdge(point, b)))))
                {
                    std::cout << "Y connection\n";
                    return true;
                }
        }
    }
    return false;
}

void BoardGrid::AddPiece(const int q, const int r)
{
    throw "Not implemented";
}

bool BoardGrid::IsInBoard(const int& line, const int& column) const
{
    throw "Not implemented!";
}

std::vector<GridNode> BoardGrid::NeighborModifiers() const
{
    throw "Not implemented!";
}

std::vector<GridNode> BoardGrid::GetCorners() const
{
    throw "Not implemented";
}

bool BoardGrid::IsOnEdge(const GridNode& point) const
{
    throw "Not Implemented!";
}

bool BoardGrid::CheckRingConnection(const GridNode& lastPiecePlaced) const
{ 
    throw "Not implemented!";
}

bool BoardGrid::CheckBridgeConnection(const std::shared_ptr<Player> &player) const {
    throw "Not implemented!";
}
