#include <Framework/GameChecker.h>

GameChecker::State GameChecker::Check(Game& game)
{
    if (game.WinCondition())
        return GameChecker::State::WIN;

    if (game.m_board->IsBoardFull())
        return GameChecker::State::DRAW;

    return GameChecker::State::NONE;
}