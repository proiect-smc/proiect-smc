#include <Framework/Game.h>

#include <utility>

Game::Game(std::string gameName) :
    m_name{std::move( gameName )},
    m_allowPieRule{true},
    m_pieRuleDiscussed{false},
    m_pieRuleApplied{false},
    m_allowPieceMove{false},
    m_isInMovingState{false},
    m_boardSize{0}
{
    //empty body
}

void Game::AddPlayer(const std::string& name, const Player::Color& color) {
    m_players.push_back(std::make_shared<Player>(name, color));
}

void Game::AddPlayer(const Player& player) {
    m_players.push_back(std::make_shared<Player>(player.GetPlayerName(), player.GetColor()));
}

void Game::UpdateCurrentPlayer()
{
    std::advance(m_currentPlayer, 1);
    if (m_currentPlayer == m_players.end())
        m_currentPlayer = m_players.begin();
}

void Game::SetPieRule(const bool isActive) 
{
    m_allowPieRule = isActive;
}

const bool Game::IsPieRuleActive() const noexcept 
{
    return m_allowPieRule;
}

void Game::DiscussPieRule() noexcept 
{
    m_pieRuleDiscussed = true;
}

const bool Game::IsMovePieceAllowed() const
{
    return m_allowPieceMove;
}

void Game::ApplyPieRule() 
{
    try 
    {
        if (m_players.size() != 2)
            throw "Pie rule applies only for 2 players.\n";

        if (!m_allowPieRule)
            throw "Pie rule is not allowed.\n";

        std::cout << "PieRule\n";

        auto& piecesOfFirstPlayer = m_ownedPieces[GetFirstPlayer()];

        auto& cell = piecesOfFirstPlayer.back();
        piecesOfFirstPlayer.pop_back();

        m_board->at(cell).RemoveOwner();
        OnPiecePlaced(cell);

        m_pieRuleApplied = true;
    }
    catch (const char* message)
    {
        std::cout << message;
    }
}

const bool Game::IsPieRuleDiscussed() const noexcept {
    return m_pieRuleDiscussed;
}

const bool Game::IsGameInMovingState() const
{
    return m_isInMovingState;
}

void Game::EnterMovingState()
{
    m_isInMovingState = true;
}

std::shared_ptr<Player> Game::GetCurrentPlayer() const {
    return *m_currentPlayer;
}

std::shared_ptr<Player> Game::GetFirstPlayer() const {
    return m_players.front();
}

std::shared_ptr<Player> Game::GetPlayerAt(const int index) const
{
    return m_players[index];
}

const std::vector<std::shared_ptr<Player>>& Game::GetPlayers() const
{
    return m_players;
}

const std::string& Game::GetName() const
{
    return m_name;
}

void Game::Start()
{
    std::cout << "Game started!" << std::endl;

    try
    {
        if (m_boardSize == 0)
            throw "Please set default board size for the game.\n";
        ConfigureBoard();

        if (m_players.empty())
            throw "Please initialise player defaults for the game.\n";

        m_currentPlayer = m_players.begin();
    }
    catch (const char* message)
    {
        std::cout << message;
    }

    m_pieRuleDiscussed = false;
    m_pieRuleApplied = false;
    m_isInMovingState = false;
}

void Game::Restart()
{
    std::cout << "Game restarted!" << std::endl;
    m_board->Restart();

    m_currentPlayer = m_players.begin();
    m_pieRuleDiscussed = false;
    m_pieRuleApplied = false;
    m_isInMovingState = false;
}

void Game::Stop()
{
    std::cout << "Game finished!" << std::endl;
    m_board->Restart();
}

void Game::OnPieceSelected(const GridNode& cell)
{
    try
    {
        if (m_isInMovingState) throw "Player must first place piece in hand.\n";

        if (const auto& owner = m_board->at(cell).GetOwner(); owner.has_value() && m_allowPieceMove)
        {
            if (owner.value() == *m_currentPlayer)
            {
                m_board->at(cell).RemoveOwner();

                auto& piecesOfCurrentPlayer = m_ownedPieces[*m_currentPlayer];
                auto position = std::find(piecesOfCurrentPlayer.cbegin(), piecesOfCurrentPlayer.cend(), cell);

                m_ownedPieces[*m_currentPlayer].erase(position);
            }
            else
                throw "Current player tries to move a piece that does not belong to him.\n";
        }
    }
    catch (const char* message)
    {
        throw message;
    }
}

void Game::OnPiecePlaced(const GridNode& cell)
{
    try 
    {
        m_board->at(cell).SetOwner(*m_currentPlayer);
        m_ownedPieces[*m_currentPlayer].push_back(cell);

        if (m_isInMovingState) m_isInMovingState = false;
    }
    catch (const char* message)
    {
        throw message;
    }
}

void Game::SetBoardSize(const size_t size)
{
    m_boardSize = size;
}

size_t Game::GetBoardSize() const noexcept
{
    return m_boardSize;
}

void Game::ConfigureBoard()
{
    m_placePieceCallback = std::bind(&Game::OnPiecePlaced, this, std::placeholders::_1);
    m_selectPieceCallback = std::bind(&Game::OnPieceSelected, this, std::placeholders::_1);
}

BoardGrid& Game::GetBoard() {
    return *m_board;
}

bool Game::WinCondition() const
{
    std::cout << "Checked if "<< *m_currentPlayer<<" won\n";
    throw "NotImplementedError";
}

const std::vector <GridNode>& Game::GetPieces(const std::shared_ptr<Player>& player) const
{
    return m_ownedPieces.at(player);
}

const GridNode& Game::GetLastPiece(const std::shared_ptr<Player>& player) const
{
    return GetPieces(player).back();
}

Game::~Game()
{
}