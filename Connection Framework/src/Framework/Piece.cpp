#include <utility>

#include "Framework/Piece.h"

Piece::Piece(const Piece& another) {
    m_placePieceCallback = another.m_placePieceCallback;
    m_selectPieceCallback = another.m_selectPieceCallback;
    m_owner = another.m_owner;
    m_point = another.m_point;
    m_isMarker = another.m_isMarker;
}

Piece::Piece(Piece&& another) noexcept {
    *this = std::move(another);
}

Piece& Piece::operator=(Piece&& another) noexcept {
    if (this != &another)
    {
        m_point = std::exchange(another.m_point, {});
        m_owner = std::exchange(another.m_owner, std::nullopt);

        m_placePieceCallback = std::exchange(another.m_placePieceCallback, nullptr);
        m_selectPieceCallback = std::exchange(another.m_selectPieceCallback, nullptr);

        m_isMarker = std::exchange(another.m_isMarker, false);
    }

    return *this;
}

std::optional<std::shared_ptr<Player>> Piece::GetOwner() const {
    return m_owner;
}

void Piece::SetOwner(const std::shared_ptr<Player>& player) {
    if (m_owner.has_value())
        throw "Piece is already owned.";
    m_owner = player;
}

void Piece::RemoveOwner() {
    if (!m_isMarker)
        m_owner.reset();
}

void Piece::SetAsMarker() {
    m_isMarker = true;
}

const bool Piece::IsMarker() const
{
    return m_isMarker;
}

void Piece::SetPlacePieceCallback(Callback callback) {
    m_placePieceCallback = std::move(callback);
}

void Piece::SetSelectPieceCallback(Callback callback) {
    m_selectPieceCallback = std::move(callback);
}

GridNode Piece::GetPoint() const {
    return m_point;
}

void Piece::PickPiece() {
    m_placePieceCallback(m_point);
}

void Piece::SelectPiece() {
    m_selectPieceCallback(m_point);
}
Piece::Piece(Callback placePiece, Callback selectPiece, GridNode  _point) :
        m_placePieceCallback(std::move(placePiece)),
        m_selectPieceCallback(std::move(selectPiece)),
        m_point(std::move(_point)),
        m_isMarker(false)
{
    //empty
}

bool Piece::operator==(const Piece& other) const {
    return m_owner == other.m_owner;
}

Piece& Piece::operator=(const Piece& another) {
    if (this != &another) {
        m_point = another.m_point;
        m_owner = another.m_owner;
        m_placePieceCallback = another.m_placePieceCallback;
        m_selectPieceCallback = another.m_selectPieceCallback;
    }
    return *this;
}

void Piece::SetPoint(const GridNode& other) {
    m_point = other;
}