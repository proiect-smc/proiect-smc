#include <GUI/ScreenManager/Application.h>

Application& Application::GetInstace()
{
	static Application instance;
	return instance;
}

void Application::SetApplicationModel(IApplicationModel* applicationModel)
{
	m_applicationModel = applicationModel;

	applicationModel->DefineScreens();
	applicationModel->DefineInitialScreen();
	applicationModel->DefineTransientData();

	if (applicationModel->GetInitialScreen().empty())
		throw "Initial scene undifined";

	if (applicationModel->GetApplicationScreens().empty())
		throw "Application scenes are not set";

}

void Application::Start(const std::string& appName, int windowWidth, int windowHeight)
{
	m_mainWindow = std::make_unique<QMainWindow>();
	m_mainWindow->setWindowTitle(appName.c_str());
	m_mainWindow->resize(windowWidth, windowHeight);

	m_screenManager = new ScreenManager(m_applicationModel->GetInitialScreen(),
		m_applicationModel->GetApplicationScreens(),
		m_applicationModel->GetTransientDataCollection(),
		std::move(m_mainWindow));
}

Application::~Application() {
	m_applicationModel = nullptr;
	delete m_screenManager;
}
