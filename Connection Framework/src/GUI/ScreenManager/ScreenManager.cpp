#include <GUI/ScreenManager/ScreenManager.h>

ScreenManager::ScreenManager(std::string initialScreen, std::map<std::string, IScreen*> screens,
	StringAnyMap data, std::unique_ptr<QMainWindow> mainWindow) :
	m_initialScreen(initialScreen), 
	m_applicationScreens(screens),
	m_transientData(data), 
	m_mainWindow(std::move(mainWindow)),
	m_currentScreen("")
{
	for (const auto& [screenName, screen] : m_applicationScreens) {
		QObject::connect(screen, SIGNAL(ScreenChange(const std::string&)),
			this, SLOT(PutScreenChange(const std::string&)));
	}

	SetActiveScreen(m_initialScreen);

	QCoreApplication* app = QCoreApplication::instance();
	connect(app, SIGNAL(aboutToQuit()), this, SLOT(OnLastScene()));
}

void ScreenManager::SetActiveScreen(const std::string& activeSceneName)
{
	if (m_currentScreen != activeSceneName) {

		IScreen* currentScreen = nullptr;
		if (m_currentScreen != "") {

			const auto& it = m_applicationScreens.find(m_currentScreen);
			currentScreen = it->second;

			m_mainWindow = currentScreen->GetMainWindow();
			m_transientData = currentScreen->GetTransientDataCollection();
		}

		const auto& nextScene = m_applicationScreens.find(activeSceneName)->second;
		if (nextScene) {
			if (currentScreen)
				currentScreen->Release();

			nextScene->SetMainWindow(std::move(m_mainWindow));
			nextScene->SetTransientData(m_transientData);

			nextScene->CreateScreen();
			nextScene->DrawScreen();

			nextScene->Show();

			m_currentScreen = activeSceneName;
		}
	}
}

void ScreenManager::PutScreenChange(const std::string& nextScreen)
{
	SetActiveScreen(nextScreen);
}

void ScreenManager::OnLastScene()
{
	const auto& currentScreen = m_applicationScreens.find(m_currentScreen)->second;
	if (currentScreen)
		currentScreen->Release();
}