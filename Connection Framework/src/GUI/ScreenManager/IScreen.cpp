#include <GUI/ScreenManager/IScreen.h>

IScreen::IScreen(const std::string screenName) : m_screenName(screenName)
{
	//empty body
}

void IScreen::DrawScreen()
{
	QMetaObject::connectSlotsByName(m_mainWindow.get());
}

void IScreen::Show() const
{
	m_mainWindow->show();
}

std::string IScreen::GetScreenName()
{
	return m_screenName;
}

void IScreen::SetMainWindow(std::unique_ptr<QMainWindow> mainWindow)
{
	m_mainWindow = std::move(mainWindow);
}

std::unique_ptr<QMainWindow> IScreen::GetMainWindow()
{
	return std::move(m_mainWindow);
}

void IScreen::SetTransientData(StringAnyMap data) {
	if (!data.empty())
		m_transientDataCollection = std::move(data);
}

const StringAnyMap& IScreen::GetTransientDataCollection() const
{
	if (!m_transientDataCollection.empty())
		return std::move(m_transientDataCollection);
	throw "There is no transient data";
}

IScreen::~IScreen()
{
}