#include <GUI/ScreenManager/IApplicationModel.h>

IApplicationModel::IApplicationModel() : QObject(nullptr) {}

std::map<std::string, IScreen*> IApplicationModel::GetApplicationScreens() const
{
	return m_applicationScreens;
}

StringAnyMap IApplicationModel::GetTransientDataCollection() const
{
	return m_transientDataCollection;
}

std::string IApplicationModel::GetInitialScreen() const
{
	return m_intitalScreen;
}

void IApplicationModel::SetInitialScreen(const std::string& screen)
{
	m_intitalScreen = screen;
}

void IApplicationModel::AddApplicationScreenToCollection(const std::pair<std::string, IScreen*>& applicationScreen)
{
	m_applicationScreens.emplace(applicationScreen);
}

void IApplicationModel::AddTransientDataToCollection(std::pair<const std::string, const std::any> transientData)
{
	m_transientDataCollection.emplace(transientData);
}

IApplicationModel::~IApplicationModel()
{

}