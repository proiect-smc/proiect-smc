#include <GUI/Screens/ConfigureGameScreen.h>

ConfigureGameScreen::ConfigureGameScreen(const std::string& screenName) :
	IScreen(screenName)
{
}

void ConfigureGameScreen::CreateScreen()
{
	if (m_mainWindow->objectName().isEmpty())
		m_mainWindow->setObjectName(QString::fromUtf8("GameArcadeWindow"));

	m_centralWidget = std::make_unique<QWidget>(m_mainWindow.get());
	m_mainWindow->setCentralWidget(m_centralWidget.get());

	m_layout = std::make_unique<QVBoxLayout>((m_centralWidget.get()));

	m_game = std::any_cast<std::shared_ptr<Game>>(m_transientDataCollection[SELECTED_GAME]);

	m_name = std::make_unique<QLabel>(QString("Settings"));
	m_name->setAlignment(Qt::AlignCenter);
	m_nameLayout = std::make_unique<QHBoxLayout>();
	m_nameLayout->addWidget(m_name.get());

	m_gridLayout = std::make_unique<QGridLayout>();
	m_gridLayout->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum), 1, 0, 1, 1);
	m_gridLayout->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum), 1, 3, 1, 1);

	for (const auto& player : m_game->GetPlayers()) m_playersForm.push_back(std::make_unique<PlayerForm>(player, m_centralWidget.get()));

	int gridLine = 0;
	for (const auto& playerForm : m_playersForm) m_gridLayout->addWidget(playerForm.get(), ++gridLine, 1, 1, 2);

	m_boardSize = std::make_unique<QLabel>(QString("Board size"));
	m_boardSizeBox = std::make_unique<QSpinBox>();
	m_boardSizeBox->setRange(3, 15);
    m_boardSizeBox->setValue(static_cast<int>(m_game->GetBoardSize()));

	m_pieRule = std::make_unique<QLabel>(QString("Pie Rule"));
	m_pieRuleBox = std::make_unique<QCheckBox>();

	if (m_playersForm.size() > 2)
		m_pieRuleBox->setDisabled(true);

	m_gridLayout->addWidget(m_boardSize.get(), ++gridLine, 1, 1, 1);
	m_gridLayout->addWidget(m_boardSizeBox.get(), gridLine, 2, 1, 1);
	m_gridLayout->addWidget(m_pieRule.get(), ++gridLine, 1, 1, 1);
	m_gridLayout->addWidget(m_pieRuleBox.get());

	m_buttonsLayout = std::make_unique<QHBoxLayout>();

	m_buttonBack = std::make_unique<QPushButton>("Back", m_centralWidget.get());
	m_buttonStart = std::make_unique<QPushButton>("Start", m_centralWidget.get());

	m_buttonsLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
	m_buttonsLayout->addWidget(m_buttonBack.get());
	m_buttonsLayout->addWidget(m_buttonStart.get());

	m_layout->addLayout(m_nameLayout.get());
	m_layout->addItem(new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding));
	m_layout->addLayout(m_gridLayout.get());
	m_layout->addItem(new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding));
	m_layout->addLayout(m_buttonsLayout.get());

	connect(m_buttonBack.get(), &QPushButton::clicked, this, &ConfigureGameScreen::Back);
	connect(m_buttonStart.get(), &QPushButton::clicked, this, &ConfigureGameScreen::StartGame);
}

void ConfigureGameScreen::Back() 
{
	emit ScreenChange(ARCADE_SCREEN_NAME);
}

void ConfigureGameScreen::StartGame()
{
	std::for_each(m_playersForm.begin(), m_playersForm.end(), 
		[](auto& form) 
		{
			form->SavePlayerSettings(); 
		}
	);
	m_game->SetPieRule(m_pieRuleBox->isChecked());
	m_game->SetBoardSize(m_boardSizeBox->value());
	m_game->Start();

	emit ScreenChange(GAME_SCREEN_NAME);
}

void ConfigureGameScreen::Release()
{
	m_game.reset();

	m_name.reset();
	m_nameLayout.reset();

	m_playersForm.clear();
	m_boardSize.reset();
	m_pieRule.reset();

	m_boardSizeBox.reset();
	m_pieRuleBox.reset();

	m_buttonBack.reset();
	m_buttonStart.reset();
	m_buttonsLayout.reset();

	m_gridLayout.reset();

	m_layout.reset();
	m_centralWidget.reset();
}

ConfigureGameScreen::~ConfigureGameScreen()
{
}