#include <GUI/Screens/GameScreen.h>

GameScreen::GameScreen(const std::string& screenName) : 
	IScreen(screenName),
	m_gameOverPopupInitialised{false},
	m_pieRulePopupInitialised{false},
	m_removePiecePopupInitialised{false}
{
	//empty body
}

void GameScreen::InitPieRulePopup()
{
	m_pieRulePopup = std::make_unique<QMessageBox>(m_mainWindow.get());

	m_pieRulePopup->setStandardButtons(QMessageBox::Yes | QMessageBox::No);

	m_pieRulePopup->setWindowTitle("PIE RULE");
	m_pieRulePopup->setText("Apply pie rule?");
	m_pieRulePopup->setIcon(QMessageBox::Question);
	m_pieRulePopup->setWindowModality(Qt::ApplicationModal);
}

void GameScreen::InitRemovePiecePopup()
{
	m_removePiecePopup = std::make_unique<QMessageBox>(m_mainWindow.get());

	m_removePiecePopup->setStandardButtons(QMessageBox::Yes | QMessageBox::No);

	m_removePiecePopup->setWindowTitle("PIECE REMOVED");
	m_removePiecePopup->setText("Do you want to replace the piece?");
	m_removePiecePopup->setIcon(QMessageBox::Question);
	m_removePiecePopup->setWindowModality(Qt::ApplicationModal);
}

void GameScreen::InitGameOverPopup()
{
	m_gameOverPopup = std::make_unique<GameOverPopup>(m_mainWindow.get());

	std::unique_ptr<QPushButton> buttonPlayAgain = std::make_unique<QPushButton>("Play Again");
	std::unique_ptr<QPushButton> buttonBack = std::make_unique<QPushButton>("Main Menu");
	std::unique_ptr<QPushButton> buttonQuit = std::make_unique<QPushButton>("Quit");

	connect(buttonPlayAgain.get(), &QPushButton::clicked, this, &GameScreen::RestartGame);
	connect(buttonBack.get(), &QPushButton::clicked, this, &GameScreen::Back);
	connect(buttonQuit.get(), &QPushButton::clicked, this, &GameScreen::Quit);

	m_gameOverPopup->AddActionRestart(std::move(buttonPlayAgain));
	m_gameOverPopup->AddActionMainMenu(std::move(buttonBack));
	m_gameOverPopup->AddActionQuit(std::move(buttonQuit));
}

void GameScreen::CreateScreen() {
	if (m_mainWindow->objectName().isEmpty())
		m_mainWindow->setObjectName(QString::fromUtf8("GameWindow"));

	m_centralWidget = std::make_unique<QWidget>(m_mainWindow.get());
	m_mainWindow->setCentralWidget(m_centralWidget.get());

	m_layout = std::make_unique<QVBoxLayout>((m_centralWidget.get()));

	m_gameOverPopupInitialised = false;
	m_pieRulePopupInitialised = false;
	m_removePiecePopupInitialised = false;

	m_game = std::any_cast<std::shared_ptr<Game>>(m_transientDataCollection[SELECTED_GAME]);

	m_gameName = std::make_unique<QLabel>(m_game->GetName().c_str(), m_centralWidget.get());

	m_currentPlayer = std::make_unique<QLabel>(m_game->GetCurrentPlayer()->GetPlayerName().c_str() + QString("'s turn"), m_centralWidget.get());
	m_currentPlayer->setFont(QFont("Arial", 12));

	InitBoard();

	m_buttonsLayout = std::make_unique<QHBoxLayout>();

	m_buttonBack = std::make_unique<QPushButton>("Back to Menu", m_centralWidget.get());
	m_buttonRestart = std::make_unique<QPushButton>("Restart Game", m_centralWidget.get());

	m_buttonsLayout->addWidget(m_buttonBack.get());
	m_buttonsLayout->addWidget(m_buttonRestart.get());

	m_layout->addWidget(m_gameName.get());
	m_layout->addWidget(m_currentPlayer.get());
	m_layout->addWidget(m_boardWidget.get());
	m_layout->addLayout(m_buttonsLayout.get());

	connect(m_buttonRestart.get(), &QPushButton::clicked, this, &GameScreen::RestartGame);
	connect(m_buttonBack.get(), &QPushButton::clicked, this, &GameScreen::Back);
}

void GameScreen::InitBoard() {
	auto placePieceCallback = [this] { OnPiecePlaced(); };
	auto selectPieceCallback = [this] { OnPieceSelected(); };
	m_boardWidget = std::make_unique<BoardWidget>(placePieceCallback, selectPieceCallback, m_game->GetBoard(), m_game->GetName());
}

const bool GameScreen::ShouldShowPieRulePopUp() const noexcept {
	return m_game->IsPieRuleActive() && !m_game->IsPieRuleDiscussed();
}

void GameScreen::RestartGame() {
	m_game->Restart();
	m_boardWidget->Restart();
	UpdateCurrentPlayerLabel();
}

void GameScreen::OnGameOver(const GameChecker::State& state) {
	if (!m_gameOverPopupInitialised)
		InitGameOverPopup();

	m_gameOverPopup->SetMessageFromGameState(state, *m_game->GetCurrentPlayer());
	m_gameOverPopup->show();

	m_gameOverPopupInitialised = true;
}

void GameScreen::ShowPieRulePopup()
{
	if (!m_pieRulePopupInitialised)
		InitPieRulePopup();

	auto val = m_pieRulePopup->exec();

	if (val == QMessageBox::Yes) 
	{
		OnAcceptPieRule();
	}

	m_pieRulePopupInitialised = true;
}

void GameScreen::ShowRemovePiecePopup()
{
	if (!m_removePiecePopupInitialised)
		InitRemovePiecePopup();

	auto val = m_removePiecePopup->exec();

	if (val == QMessageBox::No)
	{
		m_game->UpdateCurrentPlayer();
		UpdateCurrentPlayerLabel();
	}
	else
	{
		m_game->EnterMovingState();
	}

	m_removePiecePopupInitialised = true;
}

void GameScreen::OnAcceptPieRule() {
	m_boardWidget->ApplyPieRule(m_game->GetLastPiece(m_game->GetFirstPlayer()), m_game->GetCurrentPlayer());
	m_game->ApplyPieRule();
	m_game->UpdateCurrentPlayer();
	UpdateCurrentPlayerLabel();
}

void GameScreen::Quit() {
	m_mainWindow->close();
}

void GameScreen::Back() {
	m_game->Stop();

	emit ScreenChange(ARCADE_SCREEN_NAME);
}

void GameScreen::OnPiecePlaced() 
{
	GameChecker::State currentGameState = GameChecker::Check(*m_game);
	if (currentGameState != GameChecker::State::NONE)
	{
		OnGameOver(currentGameState);
	}
	else
	{
		m_game->UpdateCurrentPlayer();
		UpdateCurrentPlayerLabel();
		if (ShouldShowPieRulePopUp()) 
		{
			m_game->DiscussPieRule();
			ShowPieRulePopup();
		}
	}
}

void GameScreen::OnPieceSelected() 
{
	if (m_game->IsMovePieceAllowed() && !m_game->IsGameInMovingState())
		ShowRemovePiecePopup();
}

void GameScreen::UpdateCurrentPlayerLabel()
{
	m_currentPlayer->setText(m_game->GetCurrentPlayer()->GetPlayerName().c_str() + QString("'s turn"));
}

void GameScreen::Release() {
	m_gameOverPopup.reset();

	m_buttonRestart.reset();
	m_buttonBack.reset();
	m_buttonsLayout.reset();

	m_currentPlayer.reset();
	m_gameName.reset();
	m_boardWidget.reset();
	m_game.reset();
	m_layout.reset();

	if (m_gameOverPopupInitialised) m_gameOverPopup.reset();
	if (m_pieRulePopupInitialised) m_pieRulePopup.reset();
	if (m_removePiecePopupInitialised) m_removePiecePopup.reset();

	m_centralWidget.reset();
}

GameScreen::~GameScreen() {
}