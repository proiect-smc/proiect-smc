#include <GUI/Screens/GameArcadeScreen.h>

GameArcadeScreen::GameArcadeScreen(const std::string& screenName) :
	IScreen(screenName)
{
}

void GameArcadeScreen::AddGame(const std::shared_ptr<Game>& game)
{
	m_gamesList.push_back(game);
}

void GameArcadeScreen::PopulateGamesList()
{
	auto& gameList = m_gamesListWidget;

	std::for_each(m_gamesList.cbegin(), m_gamesList.cend(),
		[&gameList](const auto& game)
		{
			gameList->addItem(game->GetName().c_str());
		}
	);

	m_gamesListWidget->setFont(QFont("Arial", 16));
}

void GameArcadeScreen::CreateScreen()
{
	if (m_mainWindow->objectName().isEmpty())
		m_mainWindow->setObjectName(QString::fromUtf8("GameArcadeWindow"));

	m_centralWidget = std::make_unique<QWidget>(m_mainWindow.get());
	m_mainWindow->setCentralWidget(m_centralWidget.get());

	m_layout = std::make_unique<QVBoxLayout>((m_centralWidget.get()));

	m_name = std::make_unique<QLabel>(QString("Games Arcade"));
	m_name->setAlignment(Qt::AlignCenter);
	m_nameLayout = std::make_unique<QHBoxLayout>();
	m_nameLayout->addWidget(m_name.get());

	m_gamesListWidget = std::make_unique<QListWidget>();
	PopulateGamesList();

	m_buttonsLayout = std::make_unique<QHBoxLayout>();

	m_buttonQuit = std::make_unique<QPushButton>("Quit", m_centralWidget.get());
	m_buttonPickGame = std::make_unique<QPushButton>("Pick Game", m_centralWidget.get());

	m_buttonsLayout->addWidget(m_buttonQuit.get());
	m_buttonsLayout->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
	m_buttonsLayout->addWidget(m_buttonPickGame.get());

	m_layout->addLayout(m_nameLayout.get());
	m_layout->addItem(new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding));
	m_layout->addWidget(m_gamesListWidget.get());
	m_layout->addItem(new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding));
	m_layout->addLayout(m_buttonsLayout.get());

	connect(m_buttonQuit.get(), &QPushButton::clicked, this, &GameArcadeScreen::CloseOnClick);
	connect(m_buttonPickGame.get(), &QPushButton::clicked, this, &GameArcadeScreen::PickGameClick);
}

void GameArcadeScreen::Release()
{
	m_name.reset();
	m_nameLayout.reset();

	m_gamesListWidget.reset();

	m_buttonPickGame.reset();
	m_buttonQuit.reset();
	m_buttonsLayout.reset();

	m_layout.reset();
	m_centralWidget.reset();
}

void GameArcadeScreen::CloseOnClick()
{
	m_mainWindow->close();
}

void GameArcadeScreen::LoadGame(int listWidgetIndex)
{
	m_transientDataCollection.erase(SELECTED_GAME);
	m_transientDataCollection.emplace(SELECTED_GAME, m_gamesList[listWidgetIndex]);

	emit ScreenChange(CONFIG_SCREEN_NAME);
}

void GameArcadeScreen::PickGameClick()
{
	if (!m_gamesListWidget->currentItem())
	{
		std::cout << "You must select a game\n";
		return;
	}

	LoadGame(m_gamesListWidget->currentRow());
}

GameArcadeScreen::~GameArcadeScreen()
{
}
