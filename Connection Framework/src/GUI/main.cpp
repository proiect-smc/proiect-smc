#include <QApplication>

#include <GUI/ScreenManager/Application.h>
#include <GUI/ArcadeApplicationModel.h>

int main(int argc, char* argv[])
{
    QApplication application(argc, argv);

    Application& app = Application::GetInstace();
    ArcadeApplicationModel appModel;

    app.SetApplicationModel(&appModel);
    app.Start("Connection Games Arcade", 800, 720);

    auto defaultFont = application.font();
    defaultFont.setPointSize(14);
    application.setFont(defaultFont);

    return application.exec();
}