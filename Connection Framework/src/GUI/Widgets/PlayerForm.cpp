#include <GUI/Widgets/PlayerForm.h>

PlayerForm::PlayerForm(std::shared_ptr<Player> player, QWidget* parent) :
	QWidget(parent),
	m_player{player}
{
	m_gridLayout = std::make_unique<QGridLayout>(this);

	m_playerNameLbl = std::make_unique<QLabel>(QString("Name"));
	m_playerName = std::make_unique<QLineEdit>();
	m_playerName->setPlaceholderText(QString(m_player->GetPlayerName().c_str()));

	m_playerColorLbl = std::make_unique<QLabel>(QString("Color"));
	m_playerColor = std::make_unique<QComboBox>();

	m_gridLayout->addWidget(m_playerNameLbl.get(), 1, 1, 1, 1);
	m_gridLayout->addWidget(m_playerName.get(), 1, 2, 1, 1);
	m_gridLayout->addWidget(m_playerColorLbl.get(), 2, 1, 1, 1);
	m_gridLayout->addWidget(m_playerColor.get(), 2, 2, 1, 1);

	PopulateComboBox();
}

void PlayerForm::PopulateComboBox()
{
	for (const auto& [colorName, color] : m_colorList)
	{
		m_playerColor->addItem(QString(colorName.c_str()));
	}
	m_playerColor->setCurrentText(Player::ColorToString(m_player->GetColor()).c_str());
}

PlayerForm::~PlayerForm()
{
	m_gridLayout.release();
}

void PlayerForm::SavePlayerSettings()
{
	if (auto name = m_playerName->text().toStdString(); !name.empty())
		m_player->SetName(name);
	m_player->SetColor(m_colorList[m_playerColor->currentText().toStdString()]);
}