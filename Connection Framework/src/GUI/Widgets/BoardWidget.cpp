#include <GUI/Widgets/BoardWidget.h>

#include <utility>

BoardWidget::BoardWidget(std::function<void()> placePiece, std::function<void()> selectPiece, BoardGrid& board, const std::string& gameName, QWidget* parent) :
    m_boardGrid(board),
    m_placePieceCallback(std::move(placePiece)),
    m_selectPieceCallback(std::move(selectPiece)),
    m_gameName(gameName),
	QGraphicsView(parent)
{
    m_boardScene = std::make_shared<QGraphicsScene>((this));
    InitBoard();
    setScene(m_boardScene.get());
}

void BoardWidget::resizeEvent(QResizeEvent* event) 
{
    fitInView(sceneRect().x(), 
              sceneRect().y(),
              sceneRect().width()  + boardViewPadding,
              sceneRect().height() + boardViewPadding,
              Qt::KeepAspectRatio);

    QGraphicsView::resizeEvent(event);
}

BoardWidget::OrientationMap BoardWidget::InitBoardTypes()
{
    OrientationMap types;
    types["Havannah"] = layout_flat;
    types["Hex"] = layout_pointy;
    types["Y"] = layout_pointy;

    return types;
}

void BoardWidget::InitBoard()
{
    auto& layout = m_orientationTypes.at(m_gameName);

    for (auto& [hexPoint, hex] : m_boardGrid) 
    {
        m_hexes.insert({ hexPoint, std::make_shared<HexWidget>(m_placePieceCallback, m_selectPieceCallback, hex, layout) });
        m_hexes.at(hexPoint)->setPos(m_hexes.at(hexPoint)->HexToScreen(layout));
        m_boardScene->addItem(m_hexes.at(hexPoint).get());
    }
}

void BoardWidget::Restart() 
{
    for (auto& [hexPoint, hexWidget] : m_hexes) 
    {
        hexWidget->FillShape(m_boardGrid.at(hexPoint).GetOwner());
    }
}

void BoardWidget::ApplyPieRule(const GridNode& position, std::shared_ptr<Player> player)
{
    m_hexes.at(position)->FillShape(std::move(player));
}

BoardWidget::~BoardWidget() 
{
}