#include <GUI/Widgets/HexWidget.h>

Orientation::Orientation(const double f0_, const double f1_, const double f2_, const double f3_,
    const double b0_, const double b1_, const double b2_, const double b3_,
    const double start_angle_) :
    f0(f0_), f1(f1_), f2(f2_), f3(f3_),
    b0(b0_), b1(b1_), b2(b2_), b3(b3_),
    start_angle(start_angle_) 
{

}

Orientation::Orientation(const Orientation& other)
{
    *this = other;
}

Orientation::Orientation(Orientation&& other) noexcept
{
    *this = std::move(other);
}

Orientation& Orientation::operator=(Orientation&& other) noexcept
{
    if (this != &other)
    {
        double resetValue = 0;
        f0 = std::exchange(other.f0, resetValue);
        f1 = std::exchange(other.f1, resetValue);
        f2 = std::exchange(other.f2, resetValue);
        f3 = std::exchange(other.f3, resetValue);

        b0 = std::exchange(other.b0, resetValue);
        b1 = std::exchange(other.b1, resetValue);
        b2 = std::exchange(other.b2, resetValue);
        b3 = std::exchange(other.b3, resetValue);

        start_angle = std::exchange(other.start_angle, resetValue);
    }
    return *this;
}

Orientation& Orientation::operator=(const Orientation& other)
{
    if (this != &other)
    {
        f0 = other.f0;
        f1 = other.f1;
        f2 = other.f2;
        f3 = other.f3;

        b0 = other.b0;
        b1 = other.b1;
        b2 = other.b2;
        b3 = other.b3;

        start_angle = other.start_angle;
    }
    return *this;
}

Orientation::~Orientation()
{

}

bool operator==(const Orientation &lsh, const Orientation &rhs) {
    return
            lsh.b0 == rhs.b0
            && lsh.b1 == rhs.b1
            && lsh.b2 == rhs.b2
            && lsh.b3 == rhs.b3
            && lsh.f0 == rhs.f0
            && lsh.f1 == rhs.f1
            && lsh.f2 == rhs.f2
            && lsh.f3 == rhs.f3
            && lsh.start_angle == rhs.start_angle;
}

HexWidget::HexWidget(std::function<void()> placePiece, std::function<void()> selectPiece, Piece& hex, const Orientation& orientation, QGraphicsItem* parent) :
    QGraphicsPolygonItem(parent),
    m_placePiece(std::move(placePiece)),
    m_selectPiece(std::move(selectPiece)),
    m_hex(hex)
{
    QVector<QPointF> hexPoints(noOfCorners);

    for (int hexPoint = 0; hexPoint < noOfCorners; ++hexPoint) 
    {
        hexPoints[hexPoint] = GetCorner(hexPoint, orientation);
    }

    QPolygonF hexagon(hexPoints);
    setPolygon(hexagon);

    FillShape(m_hex.GetOwner());
}

const QPointF HexWidget::GetCorner(const int cornerIndex, const Orientation& orientation) const noexcept
{
    auto angleDegrees = 60 * cornerIndex - (orientation == layout_pointy ? 30 : 0);
    auto angleRadians = M_PI / 180 * angleDegrees;

    return QPointF(hexSize * cos(angleRadians),
                   hexSize * sin(angleRadians));
}

void HexWidget::mousePressEvent(QGraphicsSceneMouseEvent* event) {
	QGraphicsPolygonItem::mousePressEvent(event);
	try
	{
		if (!m_hex.GetOwner().has_value())
		{
			m_hex.PickPiece();

			FillShape(m_hex.GetOwner());
			m_placePiece();

		}
		else if (!m_hex.IsMarker())
		{
			m_hex.SelectPiece();

			FillShape(m_hex.GetOwner());
			m_selectPiece();
		}
	}
	catch (const char* message)
	{
		std::cout << message;
	}
}

const QPointF HexWidget::HexToScreen(const Orientation& orientation)  const noexcept
{
    auto h = m_hex.GetPoint();

    double x = (orientation.f0 * h.x + + orientation.f1 * h.y) * hexSize;
    double y = (orientation.f2 * h.x + + orientation.f3 * h.y) * hexSize;

    return QPointF(x, y);
}

const QColor HexWidget::GetFillColor(const Player::Color& color) const 
{
    switch (color) 
    {
    case Player::Color::BLACK:
        return Qt::black;
    case Player::Color::WHITE:
        return Qt::white;
    case Player::Color::BLUE:
        return Qt::blue;
    case Player::Color::RED:
        return Qt::red;
    case Player::Color::GREEN:
        return Qt::green;
    case Player::Color::MAGENTA:
        return Qt::magenta;
    case Player::Color::CYAN:
        return Qt::cyan;
    case Player::Color::YELLOW:
        return Qt::yellow;
    default:
        throw "Color not supported.\n";
    }
}

void HexWidget::FillShape(std::optional<std::shared_ptr<Player>> owner)
{
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::lightGray);

    if (owner.has_value())
        brush.setColor(GetFillColor(owner.value()->GetColor()));

    setBrush(brush);
}

void HexWidget::FillShape(std::shared_ptr<Player> owner)
{
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(GetFillColor(owner->GetColor()));

    setBrush(brush);
}

HexWidget::~HexWidget()
{
}

