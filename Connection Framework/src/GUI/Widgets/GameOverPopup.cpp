#include <GUI/Widgets/GameOverPopup.h>

GameOverPopup::GameOverPopup(QWidget* parent) :
	QMessageBox(parent)
{
	setWindowTitle(QString("GAME OVER"));
    setIcon(QMessageBox::Information);
    setWindowModality(Qt::ApplicationModal);
}

QString GameOverPopup::InterpretState(const GameChecker::State& state, const Player& winningPlayer)
{
    std::stringstream gameOverMessage;
    switch (state)
    {
    case GameChecker::State::WIN:
        gameOverMessage << winningPlayer.GetPlayerName();
        gameOverMessage << " won!";
        break;
    case GameChecker::State::DRAW:
        gameOverMessage << "It's a draw.";
        break;
    default: 
        break;
    }
    return QString(gameOverMessage.str().c_str());
}

void GameOverPopup::AddActionRestart(std::unique_ptr<QPushButton> button)
{
    m_buttonRestart = std::move(button);
    addButton(m_buttonRestart.get(), QMessageBox::DestructiveRole);
}

void GameOverPopup::AddActionQuit(std::unique_ptr<QPushButton> button)
{
    m_buttonQuit = std::move(button);
    addButton(m_buttonQuit.get(), QMessageBox::DestructiveRole);
}

void GameOverPopup::AddActionMainMenu(std::unique_ptr<QPushButton> button)
{
    m_buttonMainMenu = std::move(button);
    addButton(m_buttonMainMenu.get(), QMessageBox::DestructiveRole);
}

void GameOverPopup::SetMessageFromGameState(const GameChecker::State& state, const Player& winningPlayer)
{
	setText("Game over!");
    QString gameStatus = InterpretState(state, winningPlayer);
    setInformativeText(gameStatus);
}

GameOverPopup::~GameOverPopup()
{

}