#include <GUI/ArcadeApplicationModel.h>

ArcadeApplicationModel::ArcadeApplicationModel() : IApplicationModel() {
};

void ArcadeApplicationModel::DefineScreens()
{
	m_arcadeScreen = std::make_unique<GameArcadeScreen>(ARCADE_SCREEN_NAME);
	AddApplicationScreenToCollection({ ARCADE_SCREEN_NAME, m_arcadeScreen.get() });

	m_arcadeScreen->AddGame(std::make_shared<GameHavannah>());
	m_arcadeScreen->AddGame(std::make_shared<GameHex>());
	m_arcadeScreen->AddGame(std::make_shared<GameY>());

	m_configScreen = std::make_unique<ConfigureGameScreen>(CONFIG_SCREEN_NAME);
	AddApplicationScreenToCollection({ CONFIG_SCREEN_NAME, m_configScreen.get() });

	m_gameScreen = std::make_unique<GameScreen>(GAME_SCREEN_NAME);
	AddApplicationScreenToCollection({ GAME_SCREEN_NAME, m_gameScreen.get() });
}

void ArcadeApplicationModel::DefineInitialScreen()
{
	m_intitalScreen = ARCADE_SCREEN_NAME;
}

void ArcadeApplicationModel::DefineTransientData()
{
	std::shared_ptr<Game> game;
	AddTransientDataToCollection({ SELECTED_GAME, game });
}

ArcadeApplicationModel::~ArcadeApplicationModel()
{
	m_arcadeScreen.reset();
	m_configScreen.reset();
	m_gameScreen.reset();
}
