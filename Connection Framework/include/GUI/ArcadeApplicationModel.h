#ifndef ARCADEAPPLICATIONMODEL_H
#define ARCADEAPPLICATIONMODEL_H

#include <GUI/ScreenManager/IApplicationModel.h>
#include <GUI/Screens/GameScreen.h>
#include <GUI/Screens/ConfigureGameScreen.h>
#include <GUI/Screens/GameArcadeScreen.h>
#include <GUI/ScreenManager/common.h>
#include <GUI/constants.h>

class ArcadeApplicationModel : public IApplicationModel {
public:
	ArcadeApplicationModel();
	void DefineScreens() override;
	void DefineInitialScreen() override;
	void DefineTransientData() override;
	~ArcadeApplicationModel() override;

private:
	std::unique_ptr<GameArcadeScreen>	 m_arcadeScreen;
	std::unique_ptr<ConfigureGameScreen> m_configScreen;
	std::unique_ptr<GameScreen>			 m_gameScreen;
};

#endif //ARCADEAPPLICATIONMODEL_H