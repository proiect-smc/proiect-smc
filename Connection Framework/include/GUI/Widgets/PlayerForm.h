#ifndef PLAYERFORM_H
#define PLAYERFORM_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QSpacerItem>

#include <memory>
#include <unordered_map>
#include <string>

#include <Framework/Player.h>

class PlayerForm : public QWidget
{
	Q_OBJECT
public:
	explicit PlayerForm(std::shared_ptr<Player> player, QWidget* parent = nullptr);

	void SavePlayerSettings();

	~PlayerForm();

private:
	void PopulateComboBox();

private:
	std::shared_ptr<Player>		 m_player;

	std::unique_ptr<QLabel>		 m_playerNameLbl;
	std::unique_ptr<QLabel>		 m_playerColorLbl;

	std::unique_ptr<QLineEdit>   m_playerName;
	std::unique_ptr<QComboBox>   m_playerColor;

    std::unique_ptr<QGridLayout> m_gridLayout;

	std::unordered_map<std::string, Player::Color> m_colorList = Player::GetPlayerColors();
};
#endif //PLAYERFORM_H