#ifndef GAMEOVERWINDOW_H
#define GAMEOVERWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QMessageBox>

#include <sstream>
#include <functional>
#include <memory>

#include <Framework/Game.h>
#include <Framework/GameChecker.h>

class GameOverPopup : public QMessageBox
{
	Q_OBJECT
public:
	explicit GameOverPopup(QWidget* parent = nullptr);

	void AddActionRestart(std::unique_ptr<QPushButton> button);
	void AddActionQuit(std::unique_ptr<QPushButton> button);
	void AddActionMainMenu(std::unique_ptr<QPushButton> button);

	void SetMessageFromGameState(const GameChecker::State& state, const Player& winningPlayer);
	
	~GameOverPopup();

private:
	QString InterpretState(const GameChecker::State& state, const Player& winningPlayer);

private:
	std::unique_ptr<QPushButton> m_buttonRestart;
	std::unique_ptr<QPushButton> m_buttonMainMenu;
	std::unique_ptr<QPushButton> m_buttonQuit;
};

#endif //GAMEOVERWINDOW_H