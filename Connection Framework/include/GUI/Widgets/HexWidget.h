#ifndef HEXWIDGET_H
#define HEXWIDGET_H

#include <QWidget>
#include <QGraphicsPolygonItem>
#include <QHBoxLayout>
#include <QDebug>
#include <qmath.h>

#include <Framework/HexUtilities.h>
#include <Framework/Game.h>

struct Orientation 
{
    Orientation(const double f0_, const double f1_, const double f2_, const double f3_,
                const double b0_, const double b1_, const double b2_, const double b3_,
                const double start_angle_);

    Orientation() = default;
    Orientation(const Orientation& other);
    Orientation(Orientation&& other) noexcept;

    Orientation& operator=(Orientation&& other) noexcept;
    Orientation& operator=(const Orientation& other);

    ~Orientation();

    friend bool operator==(const Orientation& lsh, const Orientation& rhs);

    double f0, f1, f2, f3;
    double b0, b1, b2, b3;
    double start_angle;
};

const Orientation layout_pointy = Orientation(sqrt(3.0),  
                                              sqrt(3.0) / 2.0, 
                                              0.0, 
                                              3.0 / 2.0,
                                              sqrt(3.0) / 3.0, 
                                              -1.0 / 3.0, 
                                              0.0, 
                                              2.0 / 3.0,    
                                              0.5);

const Orientation layout_flat = Orientation(3.0 / 2.0,
                                            0.0, 
                                            sqrt(3.0) / 2.0, 
                                            sqrt(3.0),
                                            2.0 / 3.0, 
                                            0.0, 
                                            -1.0 / 3.0, 
                                            sqrt(3.0) / 3.0,
                                            0.0);


class HexWidget : public QGraphicsPolygonItem
{
public:
    HexWidget(std::function<void()> placePiece, std::function<void()> selectPiece, Piece& hex, const Orientation& orientation, QGraphicsItem* parent = NULL);
    
    [[nodiscard]] const QPointF HexToScreen(const Orientation& orientation) const noexcept;

    void FillShape(std::optional<std::shared_ptr<Player>> owner);
    void FillShape(std::shared_ptr<Player> owner);

    ~HexWidget();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

private:
    [[nodiscard]] const QColor GetFillColor(const Player::Color& color) const;
    [[nodiscard]] const QPointF GetCorner(const int cornerIndex, const Orientation& orientation) const noexcept;

private:
    Piece&                m_hex;
    std::function<void()> m_placePiece;
    std::function<void()> m_selectPiece;

    static constexpr int  noOfCorners = 6;
    static constexpr int  hexSize = 50;
};

#endif // HEXWIDGET_H
