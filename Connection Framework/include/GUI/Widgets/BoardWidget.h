#ifndef BOARDWIDGET_H
#define BOARDWIDGET_H

#include <QObject>
#include <QGraphicsView>
#include <QtDebug>

#include <memory>
#include <map>
#include <string>

#include <Framework/BoardGrid.h>
#include <GUI/Widgets/HexWidget.h>

class BoardWidget : public QGraphicsView {
public:
	using OrientationMap = std::unordered_map<std::string, Orientation>;
public:
	BoardWidget(std::function<void()> placePiece, std::function<void()> selectPiece, BoardGrid& board, const std::string& gameName, QWidget* parent = nullptr);
	
	void ApplyPieRule(const GridNode& position, std::shared_ptr<Player> player);
	void Restart();
	
	~BoardWidget();

protected:
	void resizeEvent(QResizeEvent* event) override;

private:
	void InitBoard(); 
	static OrientationMap InitBoardTypes();

private:
	BoardGrid& 		      m_boardGrid;
	std::function<void()> m_placePieceCallback;
	std::function<void()> m_selectPieceCallback;
	const std::string&    m_gameName;

	std::shared_ptr<QGraphicsScene>    m_boardScene;
	std::map<GridNode, std::shared_ptr<HexWidget>> m_hexes;

	const OrientationMap m_orientationTypes = InitBoardTypes();

	static constexpr qreal boardViewPadding{ 100 };
};
#endif // BOARDWIDGET_H
