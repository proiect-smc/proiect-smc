#ifndef APPLICATION_H
#define APPLICATION_H

#include <QMainWindow>

#include <memory>
#include <string>

#include <GUI/ScreenManager/common.h>
#include <GUI/ScreenManager/IApplicationModel.h>
#include <GUI/ScreenManager/ScreenManager.h>

class Application {

private:
	Application() = default;
	Application& operator=(const Application&) = delete;

public:
	static Application& GetInstace();
	void SetApplicationModel(IApplicationModel* applicationModel);

	void Start(const std::string& appName, int windowWidth, int windowHeight);

	virtual ~Application();
private:
	IApplicationModel*			 m_applicationModel;
	ScreenManager*				 m_screenManager;
	std::unique_ptr<QMainWindow> m_mainWindow;
};

#endif //APPLICATION_H