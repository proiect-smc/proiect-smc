#ifndef ISCREENMANAGER_H
#define ISCREENMANAGER_H

#include <QObject>
#include <QCoreApplication>

#include <GUI/ScreenManager/IScreen.h>

class ScreenManager : public QObject {
	Q_OBJECT
public:
	ScreenManager(std::string initialScreen, std::map<std::string, IScreen*> screens,
		StringAnyMap data, std::unique_ptr<QMainWindow> mainWindow);

	void SetActiveScreen(const std::string& screen);

public slots:
	void PutScreenChange(const std::string& nextScreen);
	void OnLastScene();

private:
	std::string					    m_initialScreen;
	std::string					    m_currentScreen;
	std::map<std::string, IScreen*> m_applicationScreens;
	StringAnyMap					m_transientData;
	std::unique_ptr<QMainWindow>	m_mainWindow;
};

#endif //ISCREENMANAGER_H