#ifndef COMMON_H
#define COMMON_H

#include <map>
#include <string>
#include <any>

using StringAnyMap = std::map<const std::string, const std::any>;

#endif //COMMON_H