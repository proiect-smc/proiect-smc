#ifndef IAPPLICATIONMODEL_H
#define IAPPLICATIONMODEL_H

#include <string>
#include <map>
#include <any>

#include<GUI/ScreenManager/IScreen.h>

class IApplicationModel : public QObject {
protected:
	std::string m_intitalScreen;

	std::map<std::string, IScreen*> m_applicationScreens;
	StringAnyMap m_transientDataCollection;

public:
	IApplicationModel();
	std::map<std::string, IScreen*> GetApplicationScreens() const;
	StringAnyMap GetTransientDataCollection() const;

private:

public:
	std::string GetInitialScreen() const;

public:
	virtual void DefineScreens() = 0;
	virtual void DefineInitialScreen() = 0;
	virtual void DefineTransientData() = 0;

	virtual ~IApplicationModel();

public:
	void SetInitialScreen(const std::string& screen);
	void AddApplicationScreenToCollection(const std::pair<std::string, IScreen*>& applicationScreen);
	void AddTransientDataToCollection(std::pair<const std::string, const std::any> transientData);
};

#endif //IAPPLICATIONMODEL_H