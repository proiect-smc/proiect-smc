#ifndef ISCREEN_H
#define ISCREEN_H

#include <QMainWindow>
#include <QObject>

#include <memory>

#include <GUI/ScreenManager/common.h>

class IScreen : public QObject
{
    Q_OBJECT

signals:
    void ScreenChange(const std::string& screenName);

public:
    IScreen(const std::string screenName);

    void Show() const;

    virtual void CreateScreen() = 0;
    virtual void Release() = 0;

    virtual void DrawScreen();

    virtual std::string GetScreenName();

    void SetMainWindow(std::unique_ptr<QMainWindow> mainWindow);
    std::unique_ptr<QMainWindow> GetMainWindow();

    void SetTransientData(StringAnyMap data);
    const StringAnyMap& GetTransientDataCollection() const;

    virtual ~IScreen();

protected:
    std::unique_ptr<QMainWindow> m_mainWindow;
    const std::string            m_screenName;
    StringAnyMap                 m_transientDataCollection;
};
#endif // !ISCREEN_H