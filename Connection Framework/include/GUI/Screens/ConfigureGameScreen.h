#ifndef CONFIGUREGAMEWINDOW_H
#define CONFIGUREGAMEWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpacerItem>
#include <QGridLayout>
#include <QSpinBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QVariant>
#include <QComboBox>

#include <Framework/Game.h>

#include <GUI/ScreenManager/IScreen.h>
#include <GUI/constants.h>
#include <GUI/Widgets/PlayerForm.h>

class ConfigureGameScreen : public IScreen
{
	Q_OBJECT
public:
	explicit ConfigureGameScreen(const std::string& screenName);
	~ConfigureGameScreen();

public slots:
	void Back();
	void StartGame();

private:
	void CreateScreen() override;
	void Release() override;

private:
	std::shared_ptr<Game>        m_game;

	std::unique_ptr<QWidget>	 m_centralWidget;
	std::unique_ptr<QVBoxLayout> m_layout;

	std::unique_ptr<QLabel>		 m_name;
	std::unique_ptr<QHBoxLayout> m_nameLayout;

	std::unique_ptr<QGridLayout> m_gridLayout;

	std::vector<std::unique_ptr<PlayerForm>> m_playersForm;

	std::unique_ptr<QLabel>		 m_boardSize;
	std::unique_ptr<QLabel>		 m_pieRule;

	std::unique_ptr<QSpinBox>	 m_boardSizeBox;
	std::unique_ptr<QCheckBox>   m_pieRuleBox;

	std::unique_ptr<QHBoxLayout> m_buttonsLayout;
	std::unique_ptr<QPushButton> m_buttonBack;
	std::unique_ptr<QPushButton> m_buttonStart;
};

#endif
