#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QMainWindow>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QtDebug>

#include <memory>

#include <Framework/Game.h>
#include <Framework/GameChecker.h>

#include <GUI/ScreenManager/IScreen.h>
#include <GUI/Widgets/BoardWidget.h>
#include <GUI/Widgets/GameOverPopup.h>
#include <GUI/constants.h>

class GameScreen : public IScreen
{
    Q_OBJECT
public:
    explicit GameScreen(const std::string& screenName);
    ~GameScreen();

public slots:
    void RestartGame();
    void Back();
    void Quit();

private:
    void InitBoard();

    void InitGameOverPopup();
    void InitPieRulePopup();
    void InitRemovePiecePopup();

    [[nodiscard]] const bool ShouldShowPieRulePopUp()const noexcept;

    void OnGameOver(const GameChecker::State& state);
    void OnAcceptPieRule();

    void ShowPieRulePopup();
    void ShowRemovePiecePopup();

    void UpdateCurrentPlayerLabel();

    void OnPiecePlaced();
    void OnPieceSelected();

    void CreateScreen() override;
    void Release() override;

private:
    std::unique_ptr<QWidget>       m_centralWidget;
    std::unique_ptr<QVBoxLayout>   m_layout;
    std::shared_ptr<Game>          m_game;
    std::unique_ptr<BoardWidget>   m_boardWidget;
                                   
    std::unique_ptr<QLabel>        m_gameName;
    std::unique_ptr<QLabel>        m_currentPlayer;
                                   
    std::unique_ptr<QHBoxLayout>   m_buttonsLayout;
    std::unique_ptr<QPushButton>   m_buttonRestart;
    std::unique_ptr<QPushButton>   m_buttonBack;

    std::unique_ptr<GameOverPopup> m_gameOverPopup;
    std::unique_ptr<QMessageBox>   m_pieRulePopup;
    std::unique_ptr<QMessageBox>   m_removePiecePopup;

    bool m_gameOverPopupInitialised;
    bool m_pieRulePopupInitialised;
    bool m_removePiecePopupInitialised;
};
#endif // GAMEWINDOW_H
