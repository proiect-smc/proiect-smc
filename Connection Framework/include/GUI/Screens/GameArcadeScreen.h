#ifndef GAMEARCADEWINDOW_H
#define GAMEARCADEWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidget>
#include <QLabel>
#include <QtDebug>
#include <QSpacerItem>

#include <GUI/ScreenManager/IScreen.h>
#include <GamesArcade/Y/GameY.h>
#include <GamesArcade/Hex/GameHex.h>
#include <GamesArcade/Havannah/GameHavannah.h>
#include <GUI/constants.h>

class GameArcadeScreen : public IScreen
{
	Q_OBJECT
public:
    explicit GameArcadeScreen(const std::string& screenName);

    void AddGame(const std::shared_ptr<Game>& game);

    ~GameArcadeScreen();

public slots:
    void CloseOnClick();
    void PickGameClick();

private:
    void CreateScreen() override;
    void Release() override;

    void LoadGame(int listWidgetIndex);
    void PopulateGamesList();

private:
    std::unique_ptr<QWidget>           m_centralWidget;
    std::unique_ptr<QVBoxLayout>       m_layout;
                                       
    std::unique_ptr<QLabel>            m_name;
    std::unique_ptr<QHBoxLayout>       m_nameLayout;
                                       
    std::unique_ptr<QListWidget>       m_gamesListWidget;

    std::unique_ptr<QHBoxLayout>       m_buttonsLayout;
    std::unique_ptr<QPushButton>       m_buttonPickGame;
    std::unique_ptr<QPushButton>       m_buttonQuit;

    std::vector<std::shared_ptr<Game>> m_gamesList;
};

#endif //GAMEARCADEWINDOW_H
