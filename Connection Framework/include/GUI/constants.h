#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>

const std::string ARCADE_SCREEN_NAME = std::string("Arcade");
const std::string CONFIG_SCREEN_NAME = std::string("Game Settings");
const std::string GAME_SCREEN_NAME   = std::string("Game");

const std::string SELECTED_GAME =	   std::string("selectedGame");

#endif //CONSTANTS_H