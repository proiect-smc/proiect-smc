#ifndef GAMEHEX_H
#define GAMEHEX_H

#include <GamesArcade_export.h>

#include <Framework/Game.h>
#include <GamesArcade/Hex/Parallelogram.h>

class GAMESARCADE_EXPORT GameHex : public Game {
public:
	explicit GameHex(const std::string& gameName = "Hex");

	bool WinCondition() const override;

	void ConfigureBoard() override;
};

#endif //GAMEHEX_H