#ifndef PARALLELOGRAM_H
#define PARALLELOGRAM_H

#include <GamesArcade_export.h>
#include <Framework/HexGrid.h>

class GAMESARCADE_EXPORT Parallelogram : public HexGrid {
public:
	Parallelogram(const Callback& placePieceCallback, const Callback& selectPieceCallback, const size_t& edgeSize);
	Parallelogram(const Parallelogram& another) = delete;
	Parallelogram(const Parallelogram&& another) noexcept = delete;

	Parallelogram& operator=(const Parallelogram& another) = delete;
	Parallelogram& operator=(Parallelogram&&) noexcept = delete;

	[[nodiscard]] bool CheckBridgeConnection(const std::shared_ptr<Player> &player) const override;

protected:
	[[nodiscard]] std::vector<GridNode> GetCorners() const override;

	[[nodiscard]] bool IsInBoard(const int& line, const int& column) const override;
};

#endif //PARALLELOGRAM