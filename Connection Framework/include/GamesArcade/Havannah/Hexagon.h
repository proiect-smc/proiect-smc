#ifndef HEXAGON_H
#define HEXAGON_H

#include <GamesArcade_export.h>
#include <Framework/HexGrid.h>

class GAMESARCADE_EXPORT Hexagon : public HexGrid {
public:
	Hexagon(const Callback& placePieceCallback, const Callback& selectPieceCallback, const size_t& edgeSize);
	Hexagon(const Hexagon& another) = delete;
	Hexagon(const Hexagon&& another) noexcept = delete;

	Hexagon& operator=(const Hexagon& another) = delete;
	Hexagon& operator=(Hexagon&&) noexcept = delete;

	[[nodiscard]] bool CheckBridgeConnection(const std::shared_ptr<Player> &player) const override;
	[[nodiscard]] bool CheckRingConnection(const GridNode& lastPiecePlaced) const override;
	[[nodiscard]] bool CheckYConnection(const GridNode& lastPiecePlaced, ConsiderCorners considerCorners) const override;

protected:
	[[nodiscard]] std::vector<GridNode> GetCorners() const override;

	[[nodiscard]] bool IsInBoard(const int& line, const int& column) const override;

private:
	[[nodiscard]] bool CheckHexPointIsInConnexComponent(const std::vector<GridNode>& pointsVector, const GridNode& pointToCheck) const;
	[[nodiscard]] int FindNumberOfDifferentNeighbors(const std::vector<GridNode>& pointsVector, const GridNode& point) const;
	[[nodiscard]] bool CheckIfHexPointHasSixSameNeighbors(const std::vector<GridNode>& pointsVector) const;
	[[nodiscard]] bool CheckMiddleCellInRing(const std::vector<GridNode>& pointsVector, const std::shared_ptr<Player> player) const;
};

#endif //HEXAGON