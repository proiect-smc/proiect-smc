#ifndef GAMEHAVANNAH_H
#define GAMEHAVANNAH_H

#include <GamesArcade_export.h>

#include <Framework/Game.h>
#include <GamesArcade/Havannah/Hexagon.h>

class GAMESARCADE_EXPORT GameHavannah : public Game{
public:
    explicit GameHavannah(const std::string& gameName = "Havannah");

    bool WinCondition() const override;

    void ConfigureBoard() override;
};

#endif //GAMEHAVANNAH_H