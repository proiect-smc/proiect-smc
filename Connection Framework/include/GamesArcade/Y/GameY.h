#ifndef GAMEY_H
#define GAMEY_H

#include <GamesArcade_export.h>

#include <Framework/Game.h>
#include <GamesArcade/Y/Triangle.h>

class GAMESARCADE_EXPORT GameY : public Game{
public:
    explicit GameY(const std::string& gameName = "Y");

    bool WinCondition() const override;

    void ConfigureBoard() override;
};

#endif //GAMEHEX_H
