#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <GamesArcade_export.h>
#include <Framework/HexGrid.h>

class GAMESARCADE_EXPORT Triangle : public HexGrid {
public:
	Triangle(const Callback& placePieceCallback, const Callback& selectPieceCallback, const size_t& edgeSize);
	Triangle(const Triangle& another) = delete;
	Triangle(const Triangle&& another) noexcept = delete;

	Triangle& operator=(const Triangle& another) = delete;
	Triangle& operator=(Triangle&&) noexcept = delete;

	[[nodiscard]] bool CheckYConnection(const GridNode& lastPiecePlaced, ConsiderCorners considerCorners) const override;

protected:
	[[nodiscard]] std::vector<GridNode> GetCorners() const override;

	[[nodiscard]] bool IsInBoard(const int& line, const int& column) const override;
};

#endif //TRIANGLE