#ifndef GAMECHECKER_H
#define GAMECHECKER_H

#include <Framework_export.h>

#include <Framework/Game.h>

class FRAMEWORK_EXPORT GameChecker
{
public:
	enum class State
	{
		NONE,
		WIN,
		DRAW
	};
	static GameChecker::State Check(Game& game);
};

#endif