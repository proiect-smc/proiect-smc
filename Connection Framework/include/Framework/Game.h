#ifndef GAME_H
#define GAME_H

#include <Framework_export.h>

#include <vector>
#include <string>
#include <cstdint>
#include <memory>
#include <sstream>
#include <exception>

#include <Framework/Player.h>
#include <Framework/BoardGrid.h>

class FRAMEWORK_EXPORT GameChecker;

class FRAMEWORK_EXPORT Game {
public:
	using Callback = std::function<void(const GridNode&)>;

public:
	explicit Game(std::string gameName);

	Game(const Game& other) = delete;
	Game(Game&& other) = delete;

	Game& operator=(const Game& other) = delete;
	Game& operator=(Game&& other) = delete;

	const std::string& GetName() const;
	BoardGrid& GetBoard();

	std::shared_ptr<Player> GetCurrentPlayer() const;
	std::shared_ptr<Player> GetFirstPlayer() const;
	std::shared_ptr<Player> GetPlayerAt(const int index) const;
	const std::vector<std::shared_ptr<Player>>& GetPlayers() const;

	void Start();
	void Restart();
	void Stop();

	void SetPieRule(const bool isActive = true);
	const bool IsPieRuleActive() const noexcept;
	void DiscussPieRule() noexcept;
	const bool IsPieRuleDiscussed() const noexcept;
	void ApplyPieRule();

	const bool IsMovePieceAllowed() const;
	const bool IsGameInMovingState() const;
	void EnterMovingState();

	void SetBoardSize(const size_t size);
	size_t GetBoardSize() const noexcept;

	void AddPlayer(const std::string& name, const Player::Color& color);
	void AddPlayer(const Player& player);
	void UpdateCurrentPlayer();

	const std::vector <GridNode>& GetPieces(const std::shared_ptr<Player>& player) const;
	const GridNode& GetLastPiece(const std::shared_ptr<Player>& player) const;

	virtual ~Game();

protected:
	virtual void ConfigureBoard();
	virtual bool WinCondition() const;

	virtual void OnPieceSelected(const GridNode& cell);
	virtual void OnPiecePlaced(const GridNode& cell);

protected:
	std::string m_name;

	std::vector<std::shared_ptr<Player>>	       m_players;
	std::vector<std::shared_ptr<Player>>::iterator m_currentPlayer = m_players.begin();

	size_t 					   m_boardSize;
	std::unique_ptr<BoardGrid> m_board;

	std::unordered_map<std::shared_ptr<Player>, std::vector<GridNode>> m_ownedPieces;

	bool m_allowPieceMove;
	bool m_allowPieRule;

	bool m_pieRuleDiscussed;
	bool m_pieRuleApplied;

	bool m_isInMovingState;

	Callback	 m_placePieceCallback;
	Callback	 m_selectPieceCallback;

	friend GameChecker;
};

#endif // GAME_H
