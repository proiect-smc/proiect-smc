#ifndef CONNECTIONGAMESFRAMEWORK_PIECE_H
#define CONNECTIONGAMESFRAMEWORK_PIECE_H

#include <Framework_export.h>

#include <cstdlib>
#include <unordered_map>
#include <optional>
#include <utility>
#include <vector>
#include <cmath>
#include <functional>
#include <memory>

#include <Framework/HexUtilities.h>
#include <Framework/Player.h>

class FRAMEWORK_EXPORT Piece {
protected:
	using Callback = std::function<void(const GridNode&)>;

public:
	Piece() = default;
	Piece(Callback placePiece, Callback selectPiece, GridNode  _point);
	Piece(const Piece& another);
	Piece(Piece&& another) noexcept;

	Piece& operator=(const Piece& another);
	Piece& operator=(Piece&& another) noexcept;

	bool operator==(const Piece& other) const;

	void SetOwner(const std::shared_ptr<Player>& player);
	[[nodiscard]] std::optional<std::shared_ptr<Player>> GetOwner() const;
	void RemoveOwner();

	void SetPoint(const GridNode& other);
	[[nodiscard]] GridNode GetPoint() const;

	void SetAsMarker();
	const bool IsMarker() const;

	void SetPlacePieceCallback(Callback callback);
	void SetSelectPieceCallback(Callback callback);

	void PickPiece();
	void SelectPiece();

protected:
	std::optional<std::shared_ptr<Player>> m_owner;
	GridNode                               m_point;
	Callback                               m_placePieceCallback;
	Callback                               m_selectPieceCallback;
	bool                                   m_isMarker;
};

#endif //CONNECTIONGAMESFRAMEWORK_PIECE_H
