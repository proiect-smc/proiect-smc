#ifndef BOARDGRID_H
#define BOARDGRID_H

#include <Framework_export.h>

#include <cstdint>
#include <functional>
#include <map>
#include <algorithm>
#include <iostream>
#include <queue>

#include <Framework/HexUtilities.h>
#include <Framework/Piece.h>

class FRAMEWORK_EXPORT BoardGrid : public std::map<GridNode, Piece> {
protected:
	using Callback = std::function<void(const GridNode&)>;

public:
	enum class ConsiderCorners
	{
		TRUE,
		FALSE
	};

	BoardGrid() = default;
	explicit BoardGrid(Callback placePieceCallback, Callback selectPieceCallback, const size_t& size_);

	BoardGrid(const BoardGrid& another) = delete;
	BoardGrid(const BoardGrid&& another) noexcept = delete;

	BoardGrid& operator=(const BoardGrid& another) = delete;
	BoardGrid& operator=(BoardGrid&&) noexcept = delete;
	
	[[nodiscard]] std::vector<std::vector<GridNode>> GetEdges(ConsiderCorners considerCorners) const;

	[[nodiscard]] virtual std::vector<GridNode> GetCorners() const;
    [[nodiscard]] virtual std::vector<GridNode> NeighborModifiers() const;

	[[nodiscard]] bool IsBoardFull() const;

	void Restart();

	[[nodiscard]] virtual bool CheckBridgeConnection(const std::shared_ptr<Player> &player) const;
	[[nodiscard]] virtual bool CheckYConnection(const GridNode& lastPiecePlaced, ConsiderCorners considerCorners) const;
	[[nodiscard]] virtual bool CheckRingConnection(const GridNode& lastPiecePlaced) const;

protected:
	virtual void AddPiece(const int q, const int r);

	[[nodiscard]] bool BFS(const GridNode& start, const GridNode& end, const std::shared_ptr<Player>& player) const;
	[[nodiscard]] std::vector<GridNode> BFS(const GridNode& start, const std::shared_ptr<Player>& player) const;

	[[nodiscard]] bool IsCorner(const GridNode& point) const;

	[[nodiscard]] virtual bool IsOnEdge(const GridNode& point) const;
	[[nodiscard]] virtual bool IsInBoard(const int& line, const int& column) const;
	[[nodiscard]] bool IsOnSameEdge(const GridNode& pointToCheck, const GridNode& pointOnEdge) const;
	[[nodiscard]] bool CornerIsOnSameEdges(const GridNode& corner, const GridNode& point1, const GridNode& point2) const;

	[[nodiscard]] std::vector<GridNode> FindEdgeForPoint(const GridNode& point) const;
	[[nodiscard]] std::vector<std::vector<GridNode>> FindEdgesForCorner(const GridNode& point) const;

protected:
	Callback	 m_placePieceCallback;
	Callback	 m_selectPieceCallback;

public:
	const size_t size = 0; //distance from middle to farthest point on the hexGrid
};

#endif //BOARDGRID_H
