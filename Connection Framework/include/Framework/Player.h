#ifndef PLAYER_H
#define PLAYER_H

#pragma warning( disable : 4251 )
#include <Framework_export.h>

#include <string>
#include <cstdint>
#include <iostream>
#include <unordered_map>

class FRAMEWORK_EXPORT Player {
public:
    enum class Color : uint8_t
    {
        BLACK,
        WHITE,
        RED,
        BLUE,
        YELLOW,
        MAGENTA,
        GREEN,
        CYAN,
        
        COUNT,
        NONE
    };

public:
    Player(std::string  name, const Color& color);
  
    Player(const Player& another);
    Player(Player&& another) noexcept;

    Player& operator=(const Player& another);
    Player& operator=(Player&&) noexcept;
    
    ~Player();

    [[nodiscard]] Color GetColor() const noexcept;
    [[nodiscard]] const std::string& GetPlayerName() const noexcept;

    void SetColor(const Color& color);
    void SetName(const std::string& name);

    bool operator==(const Player& other) const;  

    static std::unordered_map<std::string, Player::Color> GetPlayerColors();
    static std::string ColorToString(const Player::Color& color);

    FRAMEWORK_EXPORT friend std::ostream& operator<<(std::ostream& out, const Player& player);

private:
    std::string m_name;
    Color       m_color;
};

#endif // PLAYER_H
