#ifndef HEXGRID_H
#define HEXGRID_H

#include <Framework_export.h>
#include <Framework/BoardGrid.h>

class FRAMEWORK_EXPORT HexGrid : public BoardGrid {
public:
	HexGrid(const Callback& placePieceCallback, const Callback& selectPieceCallback, const size_t& edgeSize);

protected:
	void AddPiece(const int q, const int r) override;

	[[nodiscard]] std::vector<GridNode> NeighborModifiers() const override;
	[[nodiscard]] bool IsOnEdge(const GridNode& point) const override;
};

#endif // HEXGRID_H