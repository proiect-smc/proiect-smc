#ifndef HEXUTILITIES_H
#define HEXUTILITIES_H

#include <Framework_export.h>

#include <cstdlib>
#include <unordered_map>
#include <optional>
#include <utility>
#include <vector>
#include <cmath>
#include <functional>

class FRAMEWORK_EXPORT GridNode {
public:
    GridNode() = default;
    GridNode(int x_, int y_) noexcept;
    GridNode(const GridNode& another);
    GridNode(GridNode&& another) noexcept;

    GridNode& operator=(const GridNode& another);
    GridNode& operator=(GridNode&&) noexcept;

    virtual ~GridNode();

    bool operator==(const GridNode& other) const;
    bool operator!=(const GridNode& other) const;

    GridNode operator+(const GridNode& other) const;
    GridNode operator+=(const GridNode& other);
    GridNode operator-(const GridNode& other) const;
    GridNode operator*(int k) const;
    GridNode operator/(int k) const;
    bool operator<(const GridNode& other) const;

    [[nodiscard]] static const std::vector<GridNode> NeighborModifiers();

public:
    int x{};
    int y{};
};

class FRAMEWORK_EXPORT HexPoint : public GridNode {
public:
    HexPoint();
    HexPoint(int x_, int y_);
    explicit HexPoint(const GridNode& node);

    HexPoint(const HexPoint& another);
    HexPoint(HexPoint&& another) noexcept;

    HexPoint& operator=(const HexPoint& another);
    HexPoint& operator=(HexPoint&&) noexcept;

    ~HexPoint();

    [[nodiscard]] int z() const noexcept;

    [[nodiscard]] static const std::vector<GridNode> NeighborModifiers();
};

#endif //HEXUTILITIES_H