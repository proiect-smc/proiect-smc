cmake_minimum_required (VERSION 3.5)

# set project name
project (ConnectionGamesFramework)

# setup version numbers
set(VERSION_MAJOR 1)
set(VERSION_MINOR 0)
set(VERSION_PATCH 0)

set(CMAKE_CXX_STANDARD 20)

# add project header path
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/libs)

# add the subdirectories
add_subdirectory(src)

file(GLOB SRCS "*.QT")
